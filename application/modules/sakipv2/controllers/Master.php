<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect(site_url());
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      redirect(site_url('sakipv2/user/dashboard'));
    }
  }

  public function user() {
    $data['title'] = 'Daftar Pengguna';
    $this->template->load('mazer', 'sakipv2/master/user-index', $data);
  }

  public function user_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(TBL__USERS.'.'.COL_USERNAME=>'asc');
    $orderables = array(null,COL_USERNAME,COL_NAME,null,null);
    $cols = array(COL_USERNAME,COL_NAME);

    $queryAll = $this->db->get(TBL__USERS);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_USERNAME) {
        $item = TBL__USERS.".".COL_USERNAME;
      }
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      if($order == COL_USERNAME) {
        $order = TBL__USERS.".".COL_USERNAME;
      }
      $this->db->order_by($order, $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('*, _users.UserName as ID')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner")
    ->get_compiled_select(TBL__USERS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('sakipv2/master/user-edit/'.$r['ID']).'" class="btn btn-sm btn-outline-primary btn-edit"><i class="fas fa-edit"></i>&nbsp;UBAH</a>&nbsp;';
      if($r[COL_ISSUSPEND]==0) {
        $htmlBtn .= '<a href="'.site_url('site/master/user-activation/'.$r['ID'].'/0').'" class="btn btn-sm btn-outline-danger btn-action"><i class="fas fa-times-circle"></i>&nbsp;SUSPEND</a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/master/user-activation/'.$r['ID'].'/1').'" class="btn btn-sm btn-outline-success btn-action"><i class="fas fa-check-circle"></i>&nbsp;AKTIFKAN</a>';
      }

      $txtUnit = '';
      if($r[COL_ROLEID]!=ROLEADMIN && !empty($r[COL_COMPANYID])) {
        $arrOPD = explode('.', $r[COL_COMPANYID]);
        $rskpd = $this->db
        ->where(array(COL_SKPDURUSAN=>$arrOPD[0], COL_SKPDBIDANG=>$arrOPD[1], COL_SKPDUNIT=>$arrOPD[2], COL_SKPDSUBUNIT=>$arrOPD[3]))
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();
        if(!empty($rskpd)) {
          $txtUnit = $rskpd[COL_SKPDNAMA];
        }
      }

      $data[] = array(
        $htmlBtn,
        $r[COL_USERNAME],
        $r[COL_NAME],
        //$r[COL_ROLENAME],
        $txtUnit,
        ($r[COL_ISSUSPEND]==0?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>')
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function user_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL__USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, akun <strong>'.$rexist[COL_EMAIL].'</strong> telah terdaftar.');
        exit();
      }

      $idUnit = null;
      if($this->input->post(COL_ROLEID)!=ROLEADMIN) {
        $rskpd = $this->db->where(COL_SKPDID, $this->input->post('IdentityNo'))->get(TBL_SAKIPV2_SKPD)->row_array();
        if(empty($rskpd)) {
          ShowJsonError('PARAMETER TIDAK VALID');
          exit();
        }
        $idUnit = implode('.', array($rskpd[COL_SKPDURUSAN],$rskpd[COL_SKPDBIDANG],$rskpd[COL_SKPDUNIT],$rskpd[COL_SKPDSUBUNIT]));
      }

      $data1 = array(
        COL_NAME=>$this->input->post(COL_NAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_COMPANYID=>$idUnit
      );
      $data2 = array(
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID=>$this->input->post(COL_ROLEID),
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL__USERS, $data2);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
        $res = $this->db->insert(TBL__USERINFORMATION, $data1);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('BERHASIL!');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $this->load->view('sakipv2/master/user-form');
    }
  }

  public function user_edit($id) {
    $ruser = GetLoggedUser();
    $data['data'] = $rdata = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->where(TBL__USERS.".".COL_USERNAME, $id)
    ->get(TBL__USERS)
    ->row_array();

    if(empty($data)) {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    if(!empty($_POST)) {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $idUnit = null;
      if($this->input->post(COL_ROLEID)!=ROLEADMIN) {
        $rskpd = $this->db->where(COL_SKPDID, $this->input->post('IdentityNo'))->get(TBL_SAKIPV2_SKPD)->row_array();
        if(empty($rskpd)) {
          ShowJsonError('PARAMETER TIDAK VALID');
          exit();
        }
        $idUnit = implode('.', array($rskpd[COL_SKPDURUSAN],$rskpd[COL_SKPDBIDANG],$rskpd[COL_SKPDUNIT],$rskpd[COL_SKPDSUBUNIT]));
      }


      $data1 = array(
        COL_NAME=>$this->input->post(COL_NAME),
        COL_COMPANYID=>$idUnit,
      );
      $data2 = array(
        COL_ROLEID=>$this->input->post(COL_ROLEID),
      );
      $_pwd = $this->input->post(COL_PASSWORD);
      if(!empty($_pwd)) {
        $data2[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERS, $data2);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
        $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERINFORMATION, $data1);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('BERHASIL!');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }

      $idUnit = null;
      if($rdata[COL_ROLEID]!=ROLEADMIN) {
        $arrOPD = explode('.', $rdata[COL_COMPANYID]);
        $rskpd = $this->db
        ->where(array(COL_SKPDURUSAN=>$arrOPD[0], COL_SKPDBIDANG=>$arrOPD[1], COL_SKPDUNIT=>$arrOPD[2], COL_SKPDSUBUNIT=>$arrOPD[3]))
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();
        if(!empty($rskpd)) {
          $idUnit = $rskpd[COL_SKPDID];
        }
      }

      $data['idUnit'] = $idUnit;
      //echo $idUnit;
      //exit();
      $this->load->view('sakipv2/master/user-form', $data);
    }
  }

  public function setting() {
    $data['title'] = 'Pengaturan';
    if (!empty($_POST)) {
      $data['data'] = $_POST;
      SetSetting(SETTING_ORG_NAME, $this->input->post(SETTING_ORG_NAME));
      SetSetting(SETTING_ORG_ADDRESS, $this->input->post(SETTING_ORG_ADDRESS));
      SetSetting(SETTING_ORG_PHONE, $this->input->post(SETTING_ORG_PHONE));
      SetSetting(SETTING_ORG_FAX, $this->input->post(SETTING_ORG_FAX));
      SetSetting(SETTING_ORG_MAIL, $this->input->post(SETTING_ORG_MAIL));

      SetSetting(SETTING_WEB_NAME, $this->input->post(SETTING_WEB_NAME));
      SetSetting(SETTING_WEB_DESC, $this->input->post(SETTING_WEB_DESC));
      SetSetting(SETTING_WEB_VERSION, $this->input->post(SETTING_WEB_VERSION));

      redirect('sakipv2/master/setting');
    } else {
      $this->template->load('mazer', 'sakipv2/master/setting', $data);
    }
  }
}
?>
