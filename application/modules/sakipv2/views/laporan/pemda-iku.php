<?php
$rpemda = $this->db
->where(COL_PMDID, $idPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rmisi = $this->db
->where(COL_IDPMD, $idPmd)
->order_by(COL_MISINO)
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();
?>

<?php
if(!empty($isCetak) && $isCetak==1) {
  ?>
  <style>
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  th, td {
    padding: 10px;
  }
  </style>
  <?php
} else {
  ?>
  <div class="row p-3">
    <div class="col-lg-12 text-right">
      <a href="<?=site_url('sakipv2/laporan/index/pemda-iku-cetak').'?idPmd='.$idPmd?>" class="btn btn-outline-primary btn-sm" target="_blank">
        <i class="far fa-print"></i>&nbsp;&nbsp;CETAK
      </a>
    </div>
  </div>
  <?php
}
?>
<h5 style="font-weight: 300; text-align: center; margin-top: 10px; margin-bottom: 10px; font-size: 12pt !important">
  INDIKATOR KINERJA UTAMA (IKU) PEMERINTAH DAERAH<br />
  <strong><?=$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR]?></strong><br />
  <?='<strong>'.strtoupper($rpemda[COL_PMDPEJABAT]).'</strong> <br /> <strong>'.strtoupper($rpemda[COL_PMDPEJABATWAKIL]).'</strong>'?><br />
</h5>
<table class="table table-bordered">
  <thead>
    <tr>
      <td colspan="5" style="text-align: center">
        VISI:<br />
        <strong style="font-style: italic">"<?=$rpemda[COL_PMDVISI]?>"</strong>
      </td>
    </tr>
    <tr style="background-color: #ffc1075e">
      <th style="width: 100px; white-space: nowrap">NO.</th>
      <th>INDIKATOR KINERJA UTAMA</th>
      <th>FORMULASI</th>
      <th>SATUAN</th>
      <th>TARGET</th>
    </tr>
  </thead>
  <tbody style="font-size: 10pt !important">
    <?php
    $no=1;
    if(!empty($rmisi)) {
      foreach($rmisi as $m) {
        $arrIKU = json_decode($m[COL_MISIIKU]);
        ?>
        <tr>
          <td style="text-align: right">
            MISI <strong><?=$m[COL_MISINO]?></strong>
          </td>
          <td colspan="4">
            <strong><?=$m[COL_MISIURAIAN]?></strong>
          </td>
        </tr>
        <?php
        if(!empty($arrIKU)) {
          foreach($arrIKU as $iku) {
            ?>
            <tr>
              <td style="text-align: right"><?=$no?>.</td>
              <td style="font-style: italic"><?=strtoupper($iku->IKUUraian)?></td>
              <td style="font-style: italic"><?=strtoupper($iku->IKUFormulasi)?></td>
              <td style="font-style: italic"><?=$iku->IKUSatuan?></td>
              <td style="font-style: italic"><?=$iku->IKUTarget?></td>
            </tr>
            <?php
            $no++;
          }
        }
      }
    } else {
      ?>
      <tr>
        <td colspan="5">
          <p style="font-style: italic; text-align: center; margin-bottom: 0 !important">BELUM ADA DATA</p>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
