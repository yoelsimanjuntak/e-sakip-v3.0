<?php
$rOptPmd = $this->db
->order_by(COL_PMDISAKTIF,'desc')
->order_by(COL_PMDTAHUNMULAI,'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();
$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

$getTahun = !empty($_GET['Tahun'])?$_GET['Tahun']:date('Y');

$rpmd = $this->db
->where(COL_PMDID, $getPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$isDPAPerubahan = '';
if(!empty($_GET['isDPAPerubahan'])) {
  $isDPAPerubahan = $_GET['isDPAPerubahan'];
}
?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="form-group row">
          <label class="control-label col-lg-3 text-end">PERIODE PEMERINTAHAN :</label>
          <div class="col-lg-8">
            <select class="form-select" name="idPmd">
              <?php
              foreach($rOptPmd as $opt) {
                ?>
                <option value="<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$opt[COL_PMDID].'&Tahun='.$getTahun?>" <?=$opt[COL_PMDID]==$getPmd?'selected':''?>>
                  <?=$opt[COL_PMDTAHUNMULAI].' s.d '.$opt[COL_PMDTAHUNAKHIR].' - '.strtoupper($opt[COL_PMDPEJABAT]).($opt['PmdIsPenjabat']==1?' (Pj.)':'')?>
                </option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-lg-3 text-end">TAHUN :</label>
          <div class="col-lg-2">
            <select class="form-select" name="Tahun">
              <?php
              if(!empty($rpmd)) {
                for($i=$rpmd[COL_PMDTAHUNMULAI]; $i<=$rpmd[COL_PMDTAHUNAKHIR]; $i++) {
                  ?>
                  <option value="<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$opt[COL_PMDID].'&Tahun='.$getTahun?>" <?=$i==$getTahun?'selected':''?>>
                    <?=$i?>
                  </option>
                  <?php
                }
              }

              ?>
            </select>
          </div>
        </div>
        <?php
        if($page=='pemda-pk') {
          ?>
          <div class="form-group row">
            <div class="offset-lg-3 pl-2">
              <div class="custom-control custom-checkbox">
                <input class="custom-control-input" type="checkbox" id="isDPAPerubahan" <?=$isDPAPerubahan?'checked="true"':''?>>
                <input type="hidden" name="isDPAPerubahan" value="<?=$isDPAPerubahan?>" />
                <label for="isDPAPerubahan" class="custom-control-label">Tampilkan sebagai PK Perubahan</label>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="card-body p-0">
        <?=$this->load->view('sakipv2/laporan/'.$page, array('idPmd'=>$getPmd,'isDPAPerubahan'=>$isDPAPerubahan))?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=idPmd],input[name=Tahun],input[name=isDPAPerubahan]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('#isDPAPerubahan').change(function() {
    if($(this).is(':checked')) {
      $('[name=isDPAPerubahan]').val("<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$getPmd.'&Tahun='.$getTahun.'&isDPAPerubahan=1'?>").trigger('change');
    } else {
      $('[name=isDPAPerubahan]').val("<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$getPmd.'&Tahun='.$getTahun?>").trigger('change');
    }
  });
});
</script>
