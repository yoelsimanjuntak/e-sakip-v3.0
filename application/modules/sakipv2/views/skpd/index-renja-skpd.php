<?php
$ruser = GetLoggedUser();

$rOptPmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDISAKTIF, 'desc')
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = $rskpd[COL_SKPDID];
$getPmd = '';
$getRenstra = '';
$getDPA = '';
$getBid = '';
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

/*if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}*/

$rOptRenstra = array();
if(!empty($getSkpd) && !empty($getPmd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $getPmd)
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rrenstra = array();
$rOptSasaranSKPD = array();
$rOptDpa = array();
$rOptBid = array();
if(!empty($getRenstra)) {
  $rrenstra = $this->db
  ->where(COL_RENSTRAID, $getRenstra)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->row_array();

  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();

  $rOptBid = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_BIDISAKTIF, 1)
  ->order_by(COL_BIDNAMA,'asc')
  ->get(TBL_SAKIPV2_BID)
  ->result_array();

  $rOptSasaranSKPD = $this->db
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDRENSTRA, $getRenstra)
  ->order_by(COL_SASARANNO, 'asc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$getBid = null;
if(!empty($_GET['idBid'])) {
  $getBid = $_GET['idBid'];
} else if(!empty($rOptBid) && $rOptBid[0][COL_BIDISAKTIF]==1) {
  $getBid = $rOptBid[0][COL_BIDID];
}
?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline card-secondary">
      <div class="card-header">
        <h4 class="card-title">Filter Dokumen</h4>
      </div>
      <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <span>Renstra</span>
            <select class="form-control" name="filterRenstra" style="width: 25vw !important">
              <?php
              foreach($rOptRenstra as $opt) {
                $isSelected = '';
                if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                  $isSelected='selected';
                }
                ?>
                <option value="<?=site_url('sakipv2/skpd/renja/'.$getSkpd).'?idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                  <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                </option>
                <?php
              }
              ?>
            </select>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <span>DPA</span>
            <select class="form-control" name="filterDPA" style="width: 25vw !important">
              <?php
              foreach($rOptDpa as $opt) {
                ?>
                <option value="<?=site_url('sakipv2/skpd/renja/'.$getSkpd).'?idRenstra='.$getRenstra.'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                  <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                </option>
                <?php
              }
              ?>
            </select>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <span>Unit Kerja</span>
            <select class="form-control" name="filterBidang" style="width: 25vw !important">
              <?php
              foreach($rOptBid as $opt) {
                ?>
                <option value="<?=site_url('sakipv2/skpd/renja/'.$getSkpd).'?idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idBid='.$opt[COL_BIDID]?>" <?=$opt[COL_BIDID]==$getBid?'selected':''?>>
                  <?=strtoupper($opt[COL_BIDNAMA])?>
                </option>
                <?php
              }
              ?>
            </select>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <?php
  if(!empty($getDPA) && !empty($getBid)) {
    ?>
    <div class="col-lg-12">
      <div class="card card-outline card-secondary">
        <div class="card-header">
          <h4 class="card-title">Program, Kegiatan dan Sub Kegiatan</h4>
        </div>
        <div class="card-body">
          <table class="table table-bordered text-sm" id="table-keg">
            <thead>
              <tr>
                <tr>
                  <th>KODE</th>
                  <th>NOMENKLATUR</th>
                  <th style="white-space: nowrap">JLH. SASARAN</th>
                  <th>ANGGARAN</th>
                  <th style="white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </tr>
            </thead>
            <tbody>
              <?php
              if(!empty($rOptSasaranSKPD)) {
                $sumPaguProgram = 0;
                foreach($rOptSasaranSKPD as $sas) {
                  $rProgram = $this->db
                  ->where(COL_IDBID, $getBid)
                  ->where(COL_IDSASARANSKPD, $sas[COL_SASARANID])
                  ->where(COL_IDDPA, $getDPA)
                  ->order_by(COL_PROGRAMKODE, 'asc')
                  ->get(TBL_SAKIPV2_BID_PROGRAM)
                  ->result_array();

                  ?>
                  <tr class="fw-bold bg-light-primary">
                    <td colspan="5"><?=strtoupper($sas[COL_SASARANURAIAN])?></td>
                  </tr>
                  <?php
                  if(!empty($rProgram)) {
                    foreach($rProgram as $r) {
                      $rSasaran = $this->db
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                      ->result_array();

                      $rKegiatan = $this->db
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->get(TBL_SAKIPV2_BID_KEGIATAN)
                      ->result_array();

                      $rPaguProgram = $this->db
                      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->select_sum(COL_SUBKEGPAGU)
                      ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                      ->row_array();

                      $sumPaguProgram += $rPaguProgram[COL_SUBKEGPAGU];
                      ?>
                      <tr class="font-weight-bold">
                        <td style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$r[COL_PROGRAMKODE]?></td>
                        <td style="vertical-align: middle"><?=strtoupper($r[COL_PROGRAMURAIAN])?></td>
                        <td class="text-right" style="vertical-align: middle"><?=number_format(count($rSasaran))?></td>
                        <td class="text-right" style="vertical-align: middle"><?=number_format($rPaguProgram[COL_SUBKEGPAGU])?></td>
                        <td style="width: 100px; white-space: nowrap; vertical-align: middle">
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-program/edit/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-xs btn-edit-program"><i class="far fa-edit"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-change-program/delete/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="PROGRAM tidak dapat dihapus jika masih terdapat data KEGIATAN terkait di dalamnya." class="btn btn-danger btn-xs btn-change-program"><i class="far fa-times-circle"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-sasaran/sasaran-prog/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="SASARAN PROGRAM" class="btn btn-success btn-xs btn-change-progsasaran"><i class="far fa-bullseye-arrow"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-program/add-kegiatan/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="TAMBAH KEGIATAN" class="btn btn-primary btn-xs btn-edit-progkegiatan"><i class="far fa-plus-circle"></i></a>
                        </td>
                      </tr>
                      <?php
                      if(!empty($rKegiatan)) {
                        foreach($rKegiatan as $k) {
                          $rSasaranKeg = $this->db
                          ->where(COL_IDKEGIATAN,$k[COL_KEGIATANID])
                          ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                          ->result_array();

                          $rSubKeg = $this->db
                          ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID,"left")
                          ->where(COL_IDKEGIATAN,$k[COL_KEGIATANID])
                          ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                          ->result_array();

                          $rpagu = $this->db
                          ->where(COL_IDKEGIATAN,$k[COL_KEGIATANID])
                          ->select_sum(COL_SUBKEGPAGU)
                          ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                          ->row_array();
                          ?>
                          <tr class="text-success">
                            <td style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$k[COL_KEGIATANKODE]?></td>
                            <td style="padding-left: 1.5rem !important; vertical-align: middle"><?=strtoupper($k[COL_KEGIATANURAIAN])?></td>
                            <td class="text-right" style="vertical-align: middle"><?=number_format(count($rSasaranKeg))?></td>
                            <td class="text-right" style="vertical-align: middle"><?=number_format($rpagu[COL_SUBKEGPAGU])?></td>
                            <td style="width: 100px; white-space: nowrap; vertical-align: middle">
                              <a href="<?=site_url('sakipv2/bidang/ajax-form-program/edit-kegiatan/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-xs btn-edit-progkegiatan"><i class="far fa-edit"></i></a>
                              <a href="<?=site_url('sakipv2/bidang/ajax-change-program/delete-kegiatan/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="KEGIATAN tidak dapat dihapus jika masih terdapat data SUB KEGIATAN terkait di dalamnya." class="btn btn-danger btn-xs btn-change-progkegiatan"><i class="far fa-times-circle"></i></a>
                              <a href="<?=site_url('sakipv2/bidang/ajax-form-sasaran/sasaran-keg/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="SASARAN KEGIATAN" class="btn btn-success btn-xs btn-change-kegsasaran"><i class="far fa-bullseye-arrow"></i></a>
                              <a href="<?=site_url('sakipv2/bidang/ajax-form-subkegiatan/add/'.$k[COL_KEGIATANID].'/'.$r[COL_IDDPA])?>" data-toggle="tooltip" data-placement="bottom" title="TAMBAH SUBKEGIATAN" class="btn btn-primary btn-xs btn-edit-kegsub"><i class="far fa-plus-circle"></i></a>
                            </td>
                          </tr>
                          <?php
                          foreach($rSubKeg as $sk) {
                            $rSasaranSubkeg = $this->db
                            ->where(COL_IDSUBKEG,$sk[COL_SUBKEGID])
                            ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                            ->result_array();
                            ?>
                            <tr class="text-primary">
                              <td style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$sk[COL_SUBKEGKODE]?></td>
                              <td style="padding-left: 2.5rem !important; vertical-align: middle"><?=strtoupper($sk[COL_SUBKEGURAIAN])?><?=!empty($sk[COL_SUBBIDNAMA])?'<br /><small class="font-italic text-secondary">'.$sk[COL_SUBBIDNAMA].'</small>':''?></td>
                              <td class="text-right" style="vertical-align: middle"><?=number_format(count($rSasaranSubkeg))?></td>
                              <td class="text-right" style="vertical-align: middle"><?=number_format($sk[COL_SUBKEGPAGU])?></td>
                              <td style="width: 100px; white-space: nowrap; vertical-align: middle">
                                <a href="<?=site_url('sakipv2/bidang/ajax-form-subkegiatan/edit/'.$sk[COL_SUBKEGID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-xs btn-edit-subkeg"><i class="far fa-edit"></i></a>
                                <a href="<?=site_url('sakipv2/subbidang/ajax-change-subkegiatan/delete/'.$sk[COL_SUBKEGID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" class="btn btn-danger btn-xs btn-change-subkeg"><i class="far fa-times-circle"></i></a>
                                <a href="<?=site_url('sakipv2/subbidang/ajax-form-sasaran/sasaran-subkeg/'.$sk[COL_SUBKEGID])?>" data-toggle="tooltip" data-placement="bottom" title="SASARAN SUBKEGIATAN" class="btn btn-success btn-xs btn-change-subkegsasaran"><i class="far fa-bullseye-arrow"></i></a>
                              </td>
                            </tr>
                            <?php
                          }
                        }
                      } else {
                        ?>
                        <tr>
                          <td colspan="5">
                            <p class="text-center text-sm font-italic mb-0">(BELUM ADA KEGIATAN)</p>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5">
                        <p class="text-center fs-italic mb-0">
                          BELUM ADA DATA
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                }
              } else {
                ?>
                <tr>
                  <td colspan="5">
                    <p class="text-center fs-italic mb-0">
                      BELUM ADA DATA
                    </p>
                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3"><p class="text-right font-italic mb-0">TOTAL</p></th>
                <th><p class="text-right font-italic mb-0"><?=number_format($sumPaguProgram)?></p></th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <div class="card-footer">
          <a href="<?=site_url('sakipv2/bidang/ajax-form-program/add/'.$getBid.'/'.$getDPA)?>" class="btn btn-primary btn-add-program font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH PROGRAM</a>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<div class="modal fade" id="modalFormProgram" role="dialog" style="z-index: 9999 !important">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM PROGRAM</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormKegiatan" role="dialog" style="z-index: 9999 !important">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM KEGIATAN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormSasaran" role="dialog" style="z-index: 9999 !important">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">DAFTAR SASARAN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormProgram = $('#modalFormProgram');
var modalFormKegiatan = $('#modalFormKegiatan');
var modalFormSasaran = $('#modalFormSasaran');
$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterDPA],select[name=filterBidang]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormProgram.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormProgram).empty();
  });

  $('.btn-add-program, .btn-edit-program').click(function() {
    var url = $(this).attr('href');
    modalFormProgram.modal('show');
    $('.modal-body', modalFormProgram).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormProgram).load(url, function(){
      $('button[type=submit]', modalFormProgram).unbind('click').click(function(){
        $('form', modalFormProgram).submit();
      });
      $("select", modalFormProgram).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('.btn-change-program, .btn-change-progkegiatan, .btn-change-subkeg').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormKegiatan.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormKegiatan).empty();
  });

  $('.btn-add-progkegiatan, .btn-edit-progkegiatan, .btn-edit-kegsub, .btn-edit-subkeg').click(function() {
    var url = $(this).attr('href');
    modalFormKegiatan.modal('show');
    $('.modal-body', modalFormKegiatan).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormKegiatan).load(url, function(){
      $('button[type=submit]', modalFormKegiatan).unbind('click').click(function(){
        $('form', modalFormKegiatan).submit();
      });

      $("select", modalFormKegiatan).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  modalFormSasaran.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSasaran).empty();
  });

  $('.btn-change-kegsasaran, .btn-change-progsasaran, .btn-change-subkegsasaran').click(function() {
    var url = $(this).attr('href');
    modalFormSasaran.modal('show');
    $('.modal-body', modalFormSasaran).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSasaran).load(url, function(){
      $('button[type=submit]', modalFormSasaran).unbind('click').click(function(){
        $('form', modalFormSasaran).submit();
      });
    });
    return false;
  });
});
</script>
