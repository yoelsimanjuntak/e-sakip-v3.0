<?php
$rskpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDNAMA, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$rpmd = $this->db
->where(COL_PMDISAKTIF,1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
?>
<div class="row align-items-stretch">
  <?php
  if(!empty($rskpd)) {
    foreach($rskpd as $r) {
      $rrenstra = array();
      $rdpa = array();
      $rprogram = array();
      $rkegiatan = array();
      $rsubkegiatan = array();
      $sumPagu = 0;
      if(!empty($rpmd)) {
        $rrenstra = $this->db
        ->where(COL_IDSKPD, $r[COL_SKPDID])
        ->where(COL_IDPEMDA, $rpmd[COL_PMDID])
        ->where(COL_RENSTRAISAKTIF, 1)
        ->order_by(COL_RENSTRATAHUN, 'desc')
        ->get(TBL_SAKIPV2_SKPD_RENSTRA)
        ->row_array();
        if(!empty($rrenstra)) {
          $rdpa = $this->db
          ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
          ->where(COL_DPAISAKTIF, 1)
          ->order_by(COL_DPATAHUN, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
          ->row_array();
        }

        if(!empty($rdpa)) {
          $rprogram = $this->db
          ->where(COL_IDDPA, $rdpa[COL_DPAID])
          ->get(TBL_SAKIPV2_BID_PROGRAM)
          ->result_array();
          foreach($rprogram as $prog) {
            $rkeg = $this->db
            ->where(COL_IDPROGRAM, $prog[COL_PROGRAMID])
            ->get(TBL_SAKIPV2_BID_KEGIATAN)
            ->result_array();
            if(!empty($rkeg)) {
              $rkegiatan = array_merge($rkegiatan, $rkeg);
            }

            foreach($rkeg as $keg) {
              $rsubkeg = $this->db
              ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
              ->result_array();
              if(!empty($rsubkeg)) {
                $rsubkegiatan = array_merge($rsubkegiatan, $rsubkeg);
              }

              foreach($rsubkeg as $subk) {
                $sumPagu += $subk[COL_SUBKEGPAGU];
              }
            }
          }

          $rsubbid = $this->db
          ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
          ->where(TBL_SAKIPV2_BID.'.'.COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
          ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
          ->where(TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDISAKTIF, 1)
          ->order_by(TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDNAMA, 'asc')
          ->get(TBL_SAKIPV2_SUBBID)
          ->result_array();
        }
      }

      ?>
      <div class="col-lg-6 py-2">
        <div class="card" style="height: 100% !important">
        <div class="card-content">
          <div class="card-body">
            <p class="fw-bold"><?=$r[COL_SKPDNAMA]?></p>
            <ul class="list-group">
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Renstra</span>
                <?=!empty($rrenstra)?'<span class="text-primary text-end fw-bold">'.$rrenstra[COL_RENSTRAURAIAN].'</span>':'<span class="text-danger text-sm fst-italic">belum terisi</span>'?>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Renja</span>
                <?=!empty($rdpa)?'<span class="text-primary text-end fw-bold">'.$rdpa[COL_DPATAHUN].' - '.$rdpa[COL_DPAURAIAN].'</span>':'<span class="text-danger text-sm fst-italic">belum terisi</span>'?>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Program</span>
                <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rprogram))?></span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Kegiatan</span>
                <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rkegiatan))?></span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Sub Kegiatan</span>
                <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rsubkegiatan))?></span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <span style="padding-right: 2.5rem !important">Pagu</span>
                <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format($sumPagu)?></span>
              </li>
            </ul>
            <div class="d-flex justify-content-end">
              <div class="btn-group mt-3" role="group">
                <a href="<?=site_url('sakipv2/skpd/renja/'.$r[COL_SKPDID])?>" class="btn btn-success btn-sm"><i class="far fa-search"></i>&nbsp;LIHAT RINCIAN</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <?php
    }
  } else {
    ?>
    <p class="text-center font-italic mb-0">
      BELUM ADA DATA TERSEDIA
    </p>
    <?php
  }
  ?>
</div>
<div class="modal fade" id="modalFormSkpd" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormSkpd = $('#modalFormSkpd');
$(document).ready(function() {
  modalFormSkpd.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSkpd).empty();
    $('.modal-title', modalFormSkpd).html('SKPD');
  });

  $('.btn-add-skpd, .btn-edit-skpd').click(function() {
    var url = $(this).attr('href');
    modalFormSkpd.modal('show');
    $('.modal-body', modalFormSkpd).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSkpd).load(url, function(){
      $('button[type=submit]', modalFormSkpd).unbind('click').click(function(){
        $('form', modalFormSkpd).submit();
      });
    });
    return false;
  });

  $( "#filterSKPD" ).keyup(function() {
    $('tr', $('tbody', $('#table-skpd'))).removeClass('d-none');
    $('tr.empty', $('tbody', $('#table-skpd'))).remove();
    var keyword = $(this).val();
    var selected = [];
    var rows = $('tr', $('tbody', $('#table-skpd')));
    $.map(rows, function(row, i) {
      var cols = $('td', $(row));
      var kode = $(cols[0]).html().replace(/(<([^>]+)>)/gi, "").trim();
      var skpd = $(cols[1]).html().replace(/(<([^>]+)>)/gi, "").trim();
      if(!kode.toLowerCase().includes(keyword.toLowerCase()) && !skpd.toLowerCase().includes(keyword.toLowerCase())) {
        $(row).addClass('d-none');
      } else {
        selected.push($(row));
      }
    });

    if(selected.length == 0) {
      $('tbody', $('#table-skpd')).append('<tr class="empty"><td colspan="4" class="text-center font-italic">DATA TIDAK DITEMUKAN</td></tr>');
    }
  });
});
</script>
