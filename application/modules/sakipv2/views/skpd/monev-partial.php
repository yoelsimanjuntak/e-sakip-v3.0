<?php
if(empty($idSKPD) && empty($idRenstra) && empty($idDPA)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DATA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rpemda)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    PERIODE PEMERINTAHAN AKTIF TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    SKPD TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();
if(empty($rrenstra)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    RENSTRA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rdpa = $this->db
->where(COL_DPAID, $idDPA)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DPA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rsasaran = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
->where(COL_IDPEMDA, $rpemda[COL_PMDID])
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDRENSTRA, $idRenstra)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$rsubkeg = $this->db
->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDDPA, $idDPA)
->order_by(COL_IDBID)
->order_by(COL_SUBKEGKODE)
->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
->result_array();
?>

<?php
if(!empty($modCetak)) {
  ?>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th {
    background: #ffc107 !important;
  }
  th, td {
    font-size: 10pt !important;
    padding: 5px 10px !important;
    max-width: 200px !important;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  </style>
  <?php

} else {
  ?>
  <style>
  td, th {
    font-size: 10pt !important;
    padding: 5px 10px !important;
  }
  .card-body.p-0 .table tbody>tr>td:first-of-type,
  .card-body.p-0 .table tbody>tr>th:first-of-type,
  .card-body.p-0 .table thead>tr>td:first-of-type,
  .card-body.p-0 .table thead>tr>th:first-of-type {
    font-size: 10pt !important;
    padding: 5px 10px !important;
  }
  td a {
    text-decoration-line: underline !important;
    text-decoration-style: dotted !important;
  }
  </style>
  <?php
}
?>
<h5 style="text-align: center; margin-top: 2rem; margin-bottom: 0 !important">
  <?=$title?><br />
  <?=strtoupper($rskpd[COL_SKPDNAMA])?><br />
  <?=in_array($idTW, array(1,2,3,4))?'TRIWULAN '.$idTW.' ':''?>TAHUN <?=$rdpa[COL_DPATAHUN]?>
</h5>
<hr />
<?php
if(empty($modCetak)) {
  ?>
  <p class="font-weight-bold font-italic pl-3 pr-3 mb-0">
    PETUNJUK: <span class="text-danger">Klik pada nilai / angka di kolom RENCANA AKSI dan REALISASI untuk mengubah nilai / angka.</span>
  </p>
  <?php
}
?>
<div class="row" style="margin-top: 1rem">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered" autosize="1">
        <thead>
          <tr>
            <th style="width: 10px; white-space: nowrap; vertical-align: middle">NO.</th>
            <th style="vertical-align: middle">SASARAN</th>
            <th style="vertical-align: middle">INDIKATOR KINERJA</th>
            <th style="width: 10px; white-space: nowrap; vertical-align: middle">SATUAN</th>
            <th class="bg-light-primary disabled" style="text-align: center">TARGET</th>
            <th class="bg-light-success disabled" style="text-align: center">REALISASI</th>
            <th class="bg-light-yellow disabled" style="text-align: center">CATATAN</th>
          </tr>

        </thead>
        <tbody>
          <?php
          if(!empty($rsasaran)) {
            $noSasaranSkpd=0;
            foreach($rsasaran as $s) {
              $noSasaranSkpd+=1;
              $rdet = $this->db
              ->where(COL_IDSASARAN, $s[COL_SASARANID])
              ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
              ->result_array();

              $rprogram = $this->db
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->order_by(COL_IDBID)
              ->order_by(COL_PROGRAMKODE)
              ->get(TBL_SAKIPV2_BID_PROGRAM)
              ->result_array();

              $rprogramInd = $this->db
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_PROGSASARAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_PROGSASARAN);

              $rkegiatan = $this->db
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_KEGIATAN);

              $rkegiatanInd = $this->db
              ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_BID_KEGSASARAN.".".COL_IDKEGIATAN,"left")
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_KEGSASARAN);
              $noInd = 1;

              $rowspan = count($rdet);
              ?>
              <tr style="background: #dedede; font-style: italic">
                <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="font-style: italic; font-weight: bold; vertical-align: middle"><?=str_pad($noSasaranSkpd,2,"0",STR_PAD_LEFT)?>.</td>
                <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="font-style: italic; font-weight: bold; vertical-align: middle"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
                <?php
                if(!empty($rdet)) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[0][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                  ->row_array();

                  if(in_array($idTW, array(1,2,3,4))) {
                    $valTarget = !empty($rval)&&!empty($rval['MonevTargetTW'.$idTW])&&$rval['MonevTargetTW'.$idTW]!=null?$rval['MonevTargetTW'.$idTW]:'';
                    $txtTarget = !empty($rval)&&!empty($rval['MonevTargetTW'.$idTW])&&$rval['MonevTargetTW'.$idTW]!=null?$rval['MonevTargetTW'.$idTW]:'(KOSONG)';
                    $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevTargetTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                    $valReal = !empty($rval)&&!empty($rval['MonevRealisasiTW'.$idTW])&&$rval['MonevRealisasiTW'.$idTW]!=null?$rval['MonevRealisasiTW'.$idTW]:'';
                    $txtReal = !empty($rval)&&!empty($rval['MonevRealisasiTW'.$idTW])&&$rval['MonevRealisasiTW'.$idTW]!=null?$rval['MonevRealisasiTW'.$idTW]:'(KOSONG)';
                    $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevRealisasiTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                    $valCatt = !empty($rval['MonevCatatanTW'.$idTW])||$rval['MonevCatatanTW'.$idTW]!=null?$rval['MonevCatatanTW'.$idTW]:'';
                    $txtCatt = !empty($rval['MonevCatatanTW'.$idTW])||$rval['MonevCatatanTW'.$idTW]!=null?$rval['MonevCatatanTW'.$idTW]:'- isi catatan -';
                    $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevCatatanTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                  } else {
                    $valTarget = !empty($rval)&&!empty($rval[COL_MONEVTARGET])&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'';
                    $txtTarget = !empty($rval)&&!empty($rval[COL_MONEVTARGET])&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'(KOSONG)';
                    $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGET.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                    $valReal = !empty($rval)&&!empty($rval[COL_MONEVREALISASI])&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'';
                    $txtReal = !empty($rval)&&!empty($rval[COL_MONEVREALISASI])&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'(KOSONG)';
                    $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASI.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                    $valCatt = !empty($rval[COL_MONEVCATATAN])||$rval[COL_MONEVCATATAN]!=null?$rval[COL_MONEVCATATAN]:'';
                    $txtCatt = !empty($rval[COL_MONEVCATATAN])||$rval[COL_MONEVCATATAN]!=null?$rval[COL_MONEVCATATAN]:'- isi catatan -';
                    $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVCATATAN.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                  }
                  ?>
                  <td style="vertical-align: top;"><?=strtoupper($rdet[0][COL_SSRINDIKATORURAIAN])?></td>
                  <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORSATUAN])?></td>

                  <td class="bg-light-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefTarget?></td>
                  <td class="bg-light-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefReal?></td>
                  <td class="bg-light-yellow disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefCatt?></td>
                  <?php
                } else {
                  ?>
                  <td colspan="6" style="text-align: center; vertical-align: middle; white-space: nowrap">(indikator belum terisi)</td>
                  <?php
                }
                ?>
              </tr>
              <?php
              if(!empty($rdet)&&count($rdet)>1) {
                for($i=1; $i<count($rdet); $i++) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[$i][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                  ->row_array();

                  if(in_array($idTW, array(1,2,3,4))) {
                    $valTarget = !empty($rval)&&!empty($rval['MonevTargetTW'.$idTW])&&$rval['MonevTargetTW'.$idTW]!=null?$rval['MonevTargetTW'.$idTW]:'';
                    $txtTarget = !empty($rval)&&!empty($rval['MonevTargetTW'.$idTW])&&$rval['MonevTargetTW'.$idTW]!=null?$rval['MonevTargetTW'.$idTW]:'(KOSONG)';
                    $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevTargetTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                    $valReal = !empty($rval)&&!empty($rval['MonevRealisasiTW'.$idTW])&&$rval['MonevRealisasiTW'.$idTW]!=null?$rval['MonevRealisasiTW'.$idTW]:'';
                    $txtReal = !empty($rval)&&!empty($rval['MonevRealisasiTW'.$idTW])&&$rval['MonevRealisasiTW'.$idTW]!=null?$rval['MonevRealisasiTW'.$idTW]:'(KOSONG)';
                    $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevRealisasiTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                    $valCatt = !empty($rval['MonevCatatanTW'.$idTW])||$rval['MonevCatatanTW'.$idTW]!=null?$rval['MonevCatatanTW'.$idTW]:'';
                    $txtCatt = !empty($rval['MonevCatatanTW'.$idTW])||$rval['MonevCatatanTW'.$idTW]!=null?$rval['MonevCatatanTW'.$idTW]:'- isi catatan -';
                    $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.'MonevCatatanTW'.$idTW.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                  } else {
                    $valTarget = !empty($rval)&&!empty($rval[COL_MONEVTARGET])&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'';
                    $txtTarget = !empty($rval)&&!empty($rval[COL_MONEVTARGET])&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'(KOSONG)';
                    $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGET.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                    $valReal = !empty($rval)&&!empty($rval[COL_MONEVREALISASI])&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'';
                    $txtReal = !empty($rval)&&!empty($rval[COL_MONEVREALISASI])&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'(KOSONG)';
                    $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASI.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                    $valCatt = !empty($rval[COL_MONEVCATATAN])||$rval[COL_MONEVCATATAN]!=null?$rval[COL_MONEVCATATAN]:'';
                    $txtCatt = !empty($rval[COL_MONEVCATATAN])||$rval[COL_MONEVCATATAN]!=null?$rval[COL_MONEVCATATAN]:'- isi catatan -';
                    $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVCATATAN.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                  }
                  ?>
                  <tr style="background: #dedede; font-style: italic">
                    <td style="vertical-align: top"><?=strtoupper($rdet[$i][COL_SSRINDIKATORURAIAN])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORSATUAN])?></td>
                    <td class="bg-light-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefTarget?></td>
                    <td class="bg-light-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefReal?></td>
                    <td class="bg-light-yellow disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefCatt?></td>
                  </tr>
                  <?php
                }
              }

              $noSasaranPrg = 0;
              foreach($rprogram as $prg) {
                $rPrgInd = $this->db
                ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
                ->order_by(COL_SASARANNO)
                ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                ->result_array();

                $rkegiatan = $this->db
                ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
                ->order_by(COL_KEGIATANKODE)
                ->get(TBL_SAKIPV2_BID_KEGIATAN)
                ->result_array();
                foreach($rPrgInd as $prgInd) {
                  $noSasaranPrg+=1;
                  ?>
                  <tr>
                    <td style="vertical-align: top"><?=str_pad($noSasaranSkpd,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranPrg,2,"0",STR_PAD_LEFT)?></td>
                    <td style="vertical-align: top"><?=strtoupper($prgInd[COL_SASARANURAIAN])?></td>
                    <?php
                    if(in_array($idTW, array(1,2,3,4))) {
                      $valTarget = !empty($prgInd['SasaranTargetTW'.$idTW])&&$prgInd['SasaranTargetTW'.$idTW]!=null?$prgInd['SasaranTargetTW'.$idTW]:'';
                      $txtTarget = !empty($prgInd['SasaranTargetTW'.$idTW])&&$prgInd['SasaranTargetTW'.$idTW]!=null?$prgInd['SasaranTargetTW'.$idTW]:'(KOSONG)';
                      $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.'SasaranTargetTW'.$idTW.'/'.$prgInd[COL_SASARANID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                      $valReal = !empty($prgInd['SasaranRealisasiTW'.$idTW])&&$prgInd['SasaranRealisasiTW'.$idTW]!=null?$prgInd['SasaranRealisasiTW'.$idTW]:'';
                      $txtReal = !empty($prgInd['SasaranRealisasiTW'.$idTW])&&$prgInd['SasaranRealisasiTW'.$idTW]!=null?$prgInd['SasaranRealisasiTW'.$idTW]:'(KOSONG)';
                      $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.'SasaranRealisasiTW'.$idTW.'/'.$prgInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                      $valCatt = !empty($prgInd['SasaranCatatanTW'.$idTW])&&$prgInd['SasaranCatatanTW'.$idTW]!=null?$prgInd['SasaranCatatanTW'.$idTW]:'';
                      $txtCatt = !empty($prgInd['SasaranCatatanTW'.$idTW])&&$prgInd['SasaranCatatanTW'.$idTW]!=null?$prgInd['SasaranCatatanTW'.$idTW]:'- isi catatan -';
                      $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.'SasaranCatatanTW'.$idTW.'/'.$prgInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                    } else {
                      $txtTarget = !empty($rPrgInd)?strtoupper($prgInd[COL_SASARANTARGET]):'-';
                      $valReal = !empty($prgInd[COL_SASARANREALISASI])&&$prgInd[COL_SASARANREALISASI]!=null?$prgInd[COL_SASARANREALISASI]:'';
                      $txtReal = !empty($prgInd[COL_SASARANREALISASI])&&$prgInd[COL_SASARANREALISASI]!=null?$prgInd[COL_SASARANREALISASI]:'(KOSONG)';
                      $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASI.'/'.$prgInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                      $valCatt = !empty($prgInd[COL_SASARANCATATAN])&&$prgInd[COL_SASARANCATATAN]!=null?$prgInd[COL_SASARANCATATAN]:'';
                      $txtCatt = !empty($prgInd[COL_SASARANCATATAN])&&$prgInd[COL_SASARANCATATAN]!=null?$prgInd[COL_SASARANCATATAN]:'- isi catatan -';
                      $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANCATATAN.'/'.$prgInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                    }
                    ?>
                    <td style="vertical-align: top"><?=strtoupper($prgInd[COL_SASARANINDIKATOR])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rPrgInd)?strtoupper($prgInd[COL_SASARANSATUAN]):'-'?></td>
                    <td class="bg-light-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefTarget?></td>
                    <td class="bg-light-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefReal?></td>
                    <td class="bg-light-yellow disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefCatt?></td>
                  </tr>
                  <?php
                }

                $noSasaranKeg = 0;
                foreach($rkegiatan as $keg) {
                  $rKegInd = $this->db
                  ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                  ->order_by(COL_SASARANNO)
                  ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                  ->result_array();

                  $rsubkeg = $this->db
                  ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                  ->order_by(COL_SUBKEGKODE)
                  ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                  ->result_array();

                  foreach($rKegInd as $kegInd) {
                    $noSasaranKeg += 1;
                    ?>
                    <tr>
                      <td style="vertical-align: top"><?=str_pad($noSasaranSkpd,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranPrg,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranKeg,2,"0",STR_PAD_LEFT)?></td>
                      <td style="vertical-align: top"><?=strtoupper($kegInd[COL_SASARANURAIAN])?></td>
                      <?php
                      if(in_array($idTW, array(1,2,3,4))) {
                        $valTarget = !empty($kegInd['SasaranTargetTW'.$idTW])&&$kegInd['SasaranTargetTW'.$idTW]!=null?$kegInd['SasaranTargetTW'.$idTW]:'';
                        $txtTarget = !empty($kegInd['SasaranTargetTW'.$idTW])&&$kegInd['SasaranTargetTW'.$idTW]!=null?$kegInd['SasaranTargetTW'.$idTW]:'(KOSONG)';
                        $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.'SasaranTargetTW'.$idTW.'/'.$kegInd[COL_SASARANID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                        $valReal = !empty($kegInd['SasaranRealisasiTW'.$idTW])&&$kegInd['SasaranRealisasiTW'.$idTW]!=null?$kegInd['SasaranRealisasiTW'.$idTW]:'';
                        $txtReal = !empty($kegInd['SasaranRealisasiTW'.$idTW])&&$kegInd['SasaranRealisasiTW'.$idTW]!=null?$kegInd['SasaranRealisasiTW'.$idTW]:'(KOSONG)';
                        $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.'SasaranRealisasiTW'.$idTW.'/'.$kegInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                        $valCatt = !empty($kegInd['SasaranCatatanTW'.$idTW])&&$kegInd['SasaranCatatanTW'.$idTW]!=null?$kegInd['SasaranCatatanTW'.$idTW]:'';
                        $txtCatt = !empty($kegInd['SasaranCatatanTW'.$idTW])&&$kegInd['SasaranCatatanTW'.$idTW]!=null?$kegInd['SasaranCatatanTW'.$idTW]:'- isi catatan -';
                        $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.'SasaranCatatanTW'.$idTW.'/'.$kegInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                      } else {
                        $txtTarget = !empty($kegInd)?strtoupper($kegInd[COL_SASARANTARGET]):'-';
                        $valReal = !empty($kegInd[COL_SASARANREALISASI])&&$kegInd[COL_SASARANREALISASI]!=null?$kegInd[COL_SASARANREALISASI]:'';
                        $txtReal = !empty($kegInd[COL_SASARANREALISASI])&&$kegInd[COL_SASARANREALISASI]!=null?$kegInd[COL_SASARANREALISASI]:'(KOSONG)';
                        $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASI.'/'.$kegInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                        $valCatt = !empty($kegInd[COL_SASARANCATATAN])&&$kegInd[COL_SASARANCATATAN]!=null?$kegInd[COL_SASARANCATATAN]:'';
                        $txtCatt = !empty($kegInd[COL_SASARANCATATAN])&&$kegInd[COL_SASARANCATATAN]!=null?$kegInd[COL_SASARANCATATAN]:'- isi catatan -';
                        $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANCATATAN.'/'.$kegInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                      }
                      ?>
                      <td style="vertical-align: top"><?=strtoupper($kegInd[COL_SASARANINDIKATOR])?></td>
                      <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($kegInd[COL_SASARANSATUAN])?></td>
                      <td class="bg-light-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefTarget?></td>
                      <td class="bg-light-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefReal?></td>
                      <td class="bg-light-yellow disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefCatt?></td>
                    </tr>
                    <?php
                  }

                  $noSasaranSubKeg = 0;
                  foreach($rsubkeg as $sub) {
                    $rSubKegInd = $this->db
                    ->where(COL_IDSUBKEG, $sub[COL_SUBKEGID])
                    ->order_by(COL_SASARANNO)
                    ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                    ->result_array();

                    foreach($rSubKegInd as $subkegInd) {
                      $noSasaranSubKeg += 1;
                      ?>
                      <tr>
                        <td style="vertical-align: top"><?=str_pad($noSasaranSkpd,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranPrg,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranKeg,2,"0",STR_PAD_LEFT).'.'.str_pad($noSasaranSubKeg,2,"0",STR_PAD_LEFT)?></td>
                        <td style="vertical-align: top"><?=strtoupper($kegInd[COL_SASARANURAIAN])?></td>
                        <?php
                        if(in_array($idTW, array(1,2,3,4))) {
                          $valTarget = !empty($subkegInd['SasaranTargetTW'.$idTW])&&$subkegInd['SasaranTargetTW'.$idTW]!=null?$subkegInd['SasaranTargetTW'.$idTW]:'';
                          $txtTarget = !empty($subkegInd['SasaranTargetTW'.$idTW])&&$subkegInd['SasaranTargetTW'.$idTW]!=null?$subkegInd['SasaranTargetTW'.$idTW]:'(KOSONG)';
                          $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.'SasaranTargetTW'.$idTW.'/'.$subkegInd[COL_SASARANID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                          $valReal = !empty($subkegInd['SasaranRealisasiTW'.$idTW])&&$subkegInd['SasaranRealisasiTW'.$idTW]!=null?$subkegInd['SasaranRealisasiTW'.$idTW]:'';
                          $txtReal = !empty($subkegInd['SasaranRealisasiTW'.$idTW])&&$subkegInd['SasaranRealisasiTW'.$idTW]!=null?$subkegInd['SasaranRealisasiTW'.$idTW]:'(KOSONG)';
                          $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.'SasaranRealisasiTW'.$idTW.'/'.$subkegInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                          $valCatt = !empty($subkegInd['SasaranCatatanTW'.$idTW])&&$subkegInd['SasaranCatatanTW'.$idTW]!=null?$subkegInd['SasaranCatatanTW'.$idTW]:'';
                          $txtCatt = !empty($subkegInd['SasaranCatatanTW'.$idTW])&&$subkegInd['SasaranCatatanTW'.$idTW]!=null?$subkegInd['SasaranCatatanTW'.$idTW]:'- isi catatan -';
                          $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.'SasaranCatatanTW'.$idTW.'/'.$subkegInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                        } else {
                          $txtTarget = !empty($subkegInd)?strtoupper($subkegInd[COL_SASARANTARGET]):'-';
                          $valReal = !empty($subkegInd[COL_SASARANREALISASI])&&$subkegInd[COL_SASARANREALISASI]!=null?$subkegInd[COL_SASARANREALISASI]:'';
                          $txtReal = !empty($subkegInd[COL_SASARANREALISASI])&&$subkegInd[COL_SASARANREALISASI]!=null?$subkegInd[COL_SASARANREALISASI]:'(KOSONG)';
                          $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASI.'/'.$subkegInd[COL_SASARANID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';

                          $valCatt = !empty($subkegInd[COL_SASARANCATATAN])&&$subkegInd[COL_SASARANCATATAN]!=null?$subkegInd[COL_SASARANCATATAN]:'';
                          $txtCatt = !empty($subkegInd[COL_SASARANCATATAN])&&$subkegInd[COL_SASARANCATATAN]!=null?$subkegInd[COL_SASARANCATATAN]:'- isi catatan -';
                          $hrefCatt = '<a class="btn-changeval" data-field="CATATAN" data-input="textarea" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANCATATAN.'/'.$subkegInd[COL_SASARANID]).'" data-value="'.$valCatt.'">'.$txtCatt.'</a>';
                        }
                        ?>
                        <td style="vertical-align: top"><?=strtoupper($subkegInd[COL_SASARANINDIKATOR])?></td>
                        <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($subkegInd[COL_SASARANSATUAN])?></td>
                        <td class="bg-light-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefTarget?></td>
                        <td class="bg-light-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefReal?></td>
                        <td class="bg-light-yellow disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$hrefCatt?></td>
                      </tr>
                      <?php
                    }
                  }
                }
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="6">
                <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                  (KOSONG)
                </p>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

});
</script>
