<?php
$ruser = GetLoggedUser();

$rOptPmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDISAKTIF, 'desc')
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = $rskpd[COL_SKPDID];
$getPmd = '';
$getRenstra = '';
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

/*if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}*/

$rOptRenstra = array();
if(!empty($getSkpd) && !empty($getPmd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $getPmd)
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rrenstra = array();
$rbidang = array();
if(!empty($getRenstra)) {
  $rrenstra = $this->db
  ->where(COL_RENSTRAID, $getRenstra)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->row_array();

  $rbidang = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->get(TBL_SAKIPV2_BID)
  ->result_array();
}

$rskpd_individu = $this->db
->where(COL_IDSUBBID, (($getSkpd+100)*-1))
->order_by(COL_PLSNAMA)
->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
->result_array();
?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline card-secondary">
      <?php
      if(!empty($rrenstra)) {
        ?>
        <div class="card-header">
          <h4 class="card-title text-center"><?=$rrenstra[COL_RENSTRAURAIAN]?></h4>
        </div>
        <?php
      }
      ?>
      <div class="card-body">
        <table id="table-skpd" class="table table-bordered">
          <thead>
            <tr>
              <th>UNIT KERJA</th>
              <th>PIMPINAN / KEPALA</th>
              <th style="width: 100px; white-space: nowrap; text-align: center !important">STATUS</th>
              <th style="width: 100px; white-space: nowrap; text-align: center !important">AKSI</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(!empty($rbidang) || !empty($rskpd_individu)) {
              foreach($rbidang as $r) {
                $rsubbid = $this->db
                ->where(COL_IDBID, $r[COL_BIDID])
                ->order_by(COL_SUBBIDNAMA)
                ->get(TBL_SAKIPV2_SUBBID)
                ->result_array();

                $rbid_individu = $this->db
                ->where(COL_IDSUBBID, (($r[COL_BIDID]+1000)*-1))
                ->order_by(COL_PLSNAMA)
                ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
                ->result_array();
                ?>
                <tr>
                  <td><?=strtoupper($r[COL_BIDNAMA])?></td>
                  <td><?=strtoupper($r[COL_BIDNAMAPIMPINAN])?></td>
                  <td class="text-center" style="white-space: nowrap;">
                    <?=$r[COL_BIDISAKTIF]==1?'<span class="badge bg-success">AKTIF</span>':'<span class="badge bg-secondary">INAKTIF</span>'?>
                  </td>
                  <td class="text-center" style="white-space: nowrap; vertical-align: middle">
                    <a href="<?=site_url('sakipv2/bidang/ajax-form-bidang/edit/'.$r[COL_BIDID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-bidang"><i class="far fa-edit"></i></a>
                    <a href="<?=site_url('sakipv2/bidang/ajax-change-bidang/delete/'.$r[COL_BIDID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="BIDANG tidak dapat dihapus jika masih terdapat data turunan terkait (PROGRAM, KEGIATAN, SUB KEGIATAN, dll) di dalamnya." class="btn btn-danger btn-sm btn-change-bidang"><i class="far fa-times-circle"></i></a>
                    <?php
                    if($r[COL_BIDISAKTIF]==1) {
                      ?>
                      <a href="<?=site_url('sakipv2/bidang/ajax-change-bidang/suspend/'.$r[COL_BIDID])?>" data-toggle="tooltip" data-placement="bottom" title="SUSPEND" class="btn btn-warning btn-sm btn-change-bidang"><i class="far fa-warning"></i></a>
                      <?php
                    } else {
                      ?>
                      <a href="<?=site_url('sakipv2/bidang/ajax-change-bidang/activate/'.$r[COL_BIDID])?>" data-toggle="tooltip" data-placement="bottom" title="AKTIFKAN" class="btn btn-success btn-sm btn-change-bidang"><i class="far fa-check-circle"></i></a>
                      <?php
                    }
                    ?>
                    <a href="<?=site_url('sakipv2/subbidang/ajax-form-subbidang/add/'.$r[COL_IDRENSTRA])?>" data-toggle="tooltip" data-placement="bottom" title="TAMBAH UNIT KERJA BAWAHAN" class="btn btn-primary btn-sm btn-add-subbidang"><i class="far fa-plus-circle"></i></a>
                  </td>
                </tr>
                <?php
                foreach($rsubbid as $sub) {
                  $rsubbid_individu = $this->db
                  ->where(COL_IDSUBBID, $sub[COL_SUBBIDID])
                  ->order_by(COL_PLSNAMA)
                  ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
                  ->result_array();
                  ?>
                  <tr>
                    <td style="padding-left: 1.5rem !important"><?=strtoupper($sub[COL_SUBBIDNAMA])?></td>
                    <td><?=strtoupper($sub[COL_SUBBIDNAMAPIMPINAN])?></td>
                    <td class="text-center" style="white-space: nowrap;">
                      <?=$sub[COL_SUBBIDISAKTIF]==1?'<span class="badge bg-success">AKTIF</span>':'<span class="badge bg-secondary">INAKTIF</span>'?>
                    </td>
                    <td style="white-space: nowrap; vertical-align: middle">
                      <a href="<?=site_url('sakipv2/subbidang/ajax-form-subbidang/edit/'.$sub[COL_SUBBIDID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-subbidang"><i class="far fa-edit"></i></a>
                      <a href="<?=site_url('sakipv2/subbidang/ajax-change-subbidang/delete/'.$sub[COL_SUBBIDID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="SUB BIDANG tidak dapat dihapus jika masih terdapat data turunan terkait (PROGRAM, KEGIATAN, SUB KEGIATAN, dll) di dalamnya." class="btn btn-danger btn-sm btn-change-subbidang"><i class="far fa-times-circle"></i></a>
                      <?php
                      if($sub[COL_SUBBIDISAKTIF]==1) {
                        ?>
                        <a href="<?=site_url('sakipv2/subbidang/ajax-change-subbidang/suspend/'.$sub[COL_SUBBIDID])?>" data-toggle="tooltip" data-placement="bottom" title="SUSPEND" class="btn btn-warning btn-sm btn-change-subbidang"><i class="far fa-warning"></i></a>
                        <?php
                      } else {
                        ?>
                        <a href="<?=site_url('sakipv2/subbidang/ajax-change-subbidang/activate/'.$sub[COL_SUBBIDID])?>" data-toggle="tooltip" data-placement="bottom" title="AKTIFKAN" class="btn btn-success btn-sm btn-change-subbidang"><i class="far fa-check-circle"></i></a>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  foreach($rsubbid_individu as $ind) {
                    ?>
                    <tr>
                      <td style="padding-left: 2.5rem !important"><?=strtoupper($ind[COL_PLSNAMA])?></td>
                      <td><?=strtoupper($ind[COL_PLSNAMAPEGAWAI])?></td>
                      <td class="text-center" style="white-space: nowrap;">
                        <span class="badge bg-success">AKTIF</span>
                      </td>
                      <td style="white-space: nowrap; vertical-align: middle">
                        <a href="<?=site_url('sakipv2/individu/ajax-form-individu/edit/'.$ind[COL_PLSID].'/'.$getRenstra)?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-individu"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/individu/ajax-change-individu/delete/'.$ind[COL_PLSID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Apakah anda yakin ingin menghapus jabatan individu berikut ini?" class="btn btn-danger btn-sm btn-change-individu"><i class="far fa-times-circle"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }

                foreach($rbid_individu as $ind) {
                  ?>
                  <tr>
                    <td style="padding-left: 1.5rem !important"><?=strtoupper($ind[COL_PLSNAMA])?></td>
                    <td><?=strtoupper($ind[COL_PLSNAMAPEGAWAI])?></td>
                    <td class="text-center" style="white-space: nowrap;">
                      <span class="badge bg-success">AKTIF</span>
                    </td>
                    <td style="white-space: nowrap; vertical-align: middle">
                      <a href="<?=site_url('sakipv2/individu/ajax-form-individu/edit/'.$ind[COL_PLSID].'/'.$getRenstra)?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-individu"><i class="far fa-edit"></i></a>
                      <a href="<?=site_url('sakipv2/individu/ajax-change-individu/delete/'.$ind[COL_PLSID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Apakah anda yakin ingin menghapus jabatan individu berikut ini?" class="btn btn-danger btn-sm btn-change-individu"><i class="far fa-times-circle"></i></a>
                    </td>
                  </tr>
                  <?php
                }
              }

              foreach($rskpd_individu as $ind) {
                ?>
                <tr>
                  <td><?=strtoupper($ind[COL_PLSNAMA])?></td>
                  <td><?=strtoupper($ind[COL_PLSNAMAPEGAWAI])?></td>
                  <td class="text-center" style="white-space: nowrap;">
                    <span class="badge bg-success">AKTIF</span>
                  </td>
                  <td style="white-space: nowrap; vertical-align: middle">
                    <a href="<?=site_url('sakipv2/individu/ajax-form-individu/edit/'.$ind[COL_PLSID].'/'.$getRenstra)?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-individu"><i class="far fa-edit"></i></a>
                    <a href="<?=site_url('sakipv2/individu/ajax-change-individu/delete/'.$ind[COL_PLSID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Apakah anda yakin ingin menghapus jabatan individu berikut ini?" class="btn btn-danger btn-sm btn-change-individu"><i class="far fa-times-circle"></i></a>
                  </td>
                </tr>
                <?php
              }
            } else {
              ?>
              <tr>
                <td colspan="4">
                  <p class="text-center fst-italic mb-0">
                    BELUM ADA DATA TERSEDIA
                  </p>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
      <?php
      if(!empty($getRenstra)) {
        ?>
        <div class="card-footer">
          <a href="<?=site_url('sakipv2/bidang/ajax-form-bidang/add/'.$getRenstra)?>" class="btn btn-primary btn-add-bidang font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH UNIT KERJA</a>
          <a href="<?=site_url('sakipv2/individu/ajax-form-individu/add/'.$getRenstra)?>" data-toggle="tooltip" data-placement="bottom" title="TAMBAH JABATAN INDIVIDU" class="btn btn-primary btn-add-individu"><i class="far fa-user-plus"></i>&nbsp;&nbsp;TAMBAH JABATAN INDIVIDU</a>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormBidang" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">BIDANG / BAGIAN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormSubbidang" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">SUB BIDANG / SUB BAGIAN / SEKSI</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormIndividu" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">JABATAN INDIVIDU</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormBidang = $('#modalFormBidang');
var modalFormSubbidang = $('#modalFormSubbidang');
var modalFormIndividu = $('#modalFormIndividu');
$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterPmd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormBidang.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormBidang).empty();
  });

  $('.btn-add-bidang, .btn-edit-bidang').click(function() {
    var url = $(this).attr('href');
    modalFormBidang.modal('show');
    $('.modal-body', modalFormBidang).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormBidang).load(url, function(){
      $('button[type=submit]', modalFormBidang).unbind('click').click(function(){
        $('form', modalFormBidang).submit();
      });
    });
    return false;
  });

  $('.btn-change-bidang').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormSubbidang.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSubbidang).empty();
  });

  $('.btn-add-subbidang, .btn-edit-subbidang').click(function() {
    var url = $(this).attr('href');
    modalFormSubbidang.modal('show');
    $('.modal-body', modalFormSubbidang).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSubbidang).load(url, function(){
      $('button[type=submit]', modalFormSubbidang).unbind('click').click(function(){
        $('form', modalFormSubbidang).submit();
      });
      $("select", modalFormSubbidang).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('.btn-change-subbidang').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  $('.btn-add-individu, .btn-edit-individu').click(function() {
    var url = $(this).attr('href');
    modalFormIndividu.modal('show');
    $('.modal-body', modalFormIndividu).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormIndividu).load(url, function(){
      $('button[type=submit]', modalFormIndividu).unbind('click').click(function(){
        $('form', modalFormIndividu).submit();
      });
      $("select", modalFormIndividu).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('.btn-change-individu').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
