<?php
$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();

$rOptTujuan = array();
if(!empty($rrenstra)) {
  $rOptTujuan = $this->db
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(TBL_SAKIPV2_PEMDA_MISI.'.'.COL_IDPMD, $rrenstra[COL_IDPEMDA])
  ->order_by(COL_MISINO)
  ->order_by(COL_TUJUANNO)
  ->get(TBL_SAKIPV2_PEMDA_TUJUAN)
  ->result_array();
}
?>
<form id="form-tujuan" action="<?=current_url()?>">
  <div class="form-group">
    <label>TUJUAN PEM. DAERAH</label>
    <select class="form-control" name="<?=COL_IDTUJUANPMD?>" style="width: 100%">
      <?php
      foreach($rOptTujuan as $opt) {
        ?>
        <option value="<?=$opt[COL_TUJUANID]?>" <?=!empty($data)&&$data[COL_IDTUJUANPMD]==$opt[COL_TUJUANID]?'selected':''?>><?='Misi '.$opt[COL_MISINO].': Tujuan '.$opt[COL_TUJUANNO].' - '.strtoupper($opt[COL_TUJUANURAIAN])?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-lg-2">
        <label>NO</label>
        <input type="number" class="form-control" name="<?=COL_TUJUANNO?>" placeholder="NO." value="<?=!empty($data)?$data[COL_TUJUANNO]:''?>" required />
      </div>
      <div class="col-lg-10">
        <label>URAIAN</label>
        <input type="text" class="form-control" name="<?=COL_TUJUANURAIAN?>" placeholder="URAIAN TUJUAN" value="<?=!empty($data)?$data[COL_TUJUANURAIAN]:''?>" required />
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('select', $('#form-tujuan')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-tujuan').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
