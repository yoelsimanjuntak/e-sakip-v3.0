<?php
$ruser = GetLoggedUser();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>

    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app-dark.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/iconly.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
    <link href="<?=base_url()?>assets/fonts/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- Datatable -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/table-datatable-jquery.css">
    <script src="<?=base_url()?>assets/themes/mazer/assets/extensions/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/themes/mazer/assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>

    <style>
    .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url()?>assets/media/preloader/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }

    @media (max-width: 767px) {
      .sidebar-toggle {
          font-size: 3vw !important;
      }
    }

    .select2-container {
      z-index: 9999 !important;
    }

    .dataTables_length .select2-container {
      z-index: 100 !important;
    }

    .modal .modal-header .close {
      border-radius: 15% !important
    }

    .text-right {
      text-align: right !important;
    }

    .font-italic, .fs-italic {
      font-style: italic;
    }

    .bg-light-primary {
      background-color: #ebf3ff !important;
      color: #002152 !important;
    }
    .bg-light-success {
        background-color: #d2ffe8 !important;
        color: #00391c !important;
    }
    .bg-light-yellow {
        background-color: #FAFFAF !important;
        color: #00391c !important;
    }
    .nowrap {
      white-space: nowrap !important;
    }
    </style>
    <script type="text/javascript">
    $(window).load(function() {
      $(".se-pre-con").fadeOut("slow");
    });
    </script>
</head>
<body>
  <script src="<?=base_url()?>assets/themes/mazer/assets/static/js/initTheme.js"></script>
  <div class="se-pre-con"></div>
  <div id="app">
    <div id="sidebar">
      <div class="sidebar-wrapper active">
        <div class="sidebar-header position-relative">
          <div class="d-flex align-items-stretch">
            <div class="logo">
              <a href="<?=site_url()?>"><img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" style="height: 2.5rem; border-radius: 15% !important"></a>
            </div>
            <div class="p-2 pl-4">
              <h5 class="mb-0" style="line-height: 1!important"><?=$this->setting_web_name?></h5>
              <p class="mb-0" style="font-size: .75rem !important"><?=$this->setting_org_name?></p>
            </div>
            <div class="sidebar-toggler x">
              <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
            </div>
          </div>
        </div>
        <div class="sidebar-menu">
          <ul class="menu">
            <li class="sidebar-title">Menu Utama</li>
            <li class="sidebar-item">
              <a href="<?=site_url('sakipv2/user/dashboard')?>" class='sidebar-link'>
                <i class="fas fa-tachometer-alt"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <?php
            if($ruser[COL_ROLEID]==ROLEADMIN) {
              ?>
              <li class="sidebar-item has-sub">
                <a href="#" class='sidebar-link'>
                  <i class="fas fa-cogs"></i>
                  <span>Master Data</span>
                </a>
                <ul class="submenu">
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/master/setting')?>" class="submenu-link">Pengaturan</a>
                  </li>
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/master/user')?>" class="submenu-link">Pengguna</a>
                  </li>
                  <!--<li class="submenu-item">
                    <a href="<?=site_url('sakipv2/master/satuan')?>" class="submenu-link">Satuan</a>
                  </li>-->
                </ul>
              </li>
              <li class="sidebar-item has-sub">
                <a href="#" class='sidebar-link'>
                  <i class="fas fa-university"></i>
                  <span>Kinerja Pemda</span>
                </a>
                <ul class="submenu">
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/pemda/index')?>" class="submenu-link">RPJMD</a>
                  </li>
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/pemda/monev')?>" class="submenu-link">Monitoring</a>
                  </li>
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/laporan/index/pemda-cascading')?>" class="submenu-link">Cascading</a>
                  </li>
                  <li class="submenu-item">
                    <a href="<?=site_url('sakipv2/laporan/index/pemda-pk')?>" class="submenu-link">Perjanjian Kinerja</a>
                  </li>
                </ul>
              </li>
              <?php
            }
            ?>

            <li class="sidebar-item has-sub">
              <a href="#" class='sidebar-link'>
                <i class="fas fa-building"></i>
                <span>Kinerja OPD</span>
              </a>
              <ul class="submenu">
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/skpd/index')?>" class="submenu-link">Rencana Strategis</a>
                </li>
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/skpd/struktur')?>" class="submenu-link">Organisasi</a>
                </li>
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/skpd/renja')?>" class="submenu-link">Rencana Kerja</a>
                </li>
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/skpd/monev')?>" class="submenu-link">Monitoring</a>
                </li>
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/laporan/index/skpd-cascading')?>" class="submenu-link">Cascading</a>
                </li>
                <li class="submenu-item">
                  <a href="<?=site_url('sakipv2/laporan/index/skpd-pk')?>" class="submenu-link">Perjanjian Kinerja</a>
                </li>
              </ul>
            </li>
            <!--<li class="sidebar-item has-sub">
              <a href="#" class='sidebar-link'>
                <i class="fas fa-folder"></i>
                <span>Arsip</span>
              </a>
              <ul class="submenu">
                <li class="submenu-item">
                  <a href="#" class="submenu-link">Pohon Kinerja</a>
                </li>
                <li class="submenu-item">
                  <a href="#" class="submenu-link">Cross Cutting</a>
                </li>
                <li class="submenu-item">
                  <a href="#" class="submenu-link">LAKIP</a>
                </li>
                <li class="submenu-item">
                  <a href="#" class="submenu-link">Lainnya</a>
                </li>
              </ul>
            </li>-->
            <li class="sidebar-title">Akun</li>
            <li class="sidebar-item">
              <a id="btn-changepass" href="<?=site_url('sakipv2/user/changepassword')?>" class='sidebar-link'>
                <i class="fas fa-lock"></i>
                <span>Ubah Password</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a href="<?=site_url('sakipv2/user/logout')?>" class='sidebar-link'>
                <i class="fas fa-sign-out"></i>
                <span>Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main" class="layout-navbar navbar-fixed">
      <header class="d-none">
        <nav class="navbar navbar-expand navbar-light navbar-top">
          <div class="container-fluid">
            <a href="#" class="burger-btn d-block">
              <i class="bi bi-justify fs-3"></i>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <div class="dropdown ms-auto">
                <a href="#" data-bs-toggle="dropdown" aria-expanded="false">
                  <div class="user-menu d-flex">
                    <div class="user-name text-end me-3">
                      <h6 class="mb-0 text-gray-600"><?=$ruser[COL_NAME]?></h6>
                      <p class="mb-0 text-sm text-gray-600"><?=$ruser[COL_ROLENAME]?></p>
                    </div>
                    <div class="user-img d-flex align-items-center">
                      <div class="avatar avatar-md">
                        <img src="<?=MY_IMAGEURL?>user.jpg">
                      </div>
                    </div>
                  </div>
                </a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton" style="min-width: 11rem;">
                  <li>
                    <a class="dropdown-item" href="#"><i class="icon-mid bi bi-lock me-2"></i> Ubah Password</a>
                  </li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="<?=site_url('site/user/logout')?>"><i class="icon-mid bi bi-box-arrow-left me-2"></i> Logout</a></li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </header>
      <div id="main-content">
        <div class="page-heading">
          <div class="page-title">
              <div class="row">
                <div class="col-12 col-md-8 order-md-1 order-last">
                  <h3><?=$title?></h3>
                  <?php
                  if(isset($subtitle)) {
                    ?>
                    <p class="text-subtitle text-muted"><?=$subtitle?></p>
                    <?php
                  }
                  ?>
                </div>
                <div class="col-12 col-md-4 order-md-2 order-first">
                  <?php
                  if(isset($urlprev)) {
                    ?>
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$urlprev?>"><i class="far fa-arrow-left"></i> KEMBALI</a></li>
                      </ol>
                    </nav>
                    <?php
                  }
                  ?>
                </div>
              </div>
          </div>
        </div>
        <div class="page-content">
          <?=$content?>
        </div>
      </div>
      <footer>
        <div class="footer clearfix mb-0 text-muted">
          <div class="float-start">
            <strong>&copy; <?=date("Y")?> <?=$this->setting_web_name?> <?=$this->setting_web_version?></strong>
          </div>
          <div class="float-end">
            <p>Developed by <a target="_blank" href="https://daksastudio.id/">Partopi <strong>Tao</strong></a></p>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="modal fade" id="modal-changepass" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">UBAH PASSWORD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="fa fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          Loading...
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="<?=base_url()?>assets/themes/mazer/assets/static/js/components/dark.js"></script>
  <script src="<?=base_url()?>assets/themes/mazer/assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mazer/assets/compiled/js/app.js"></script>

  <!-- Block UI -->
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.js"></script>

  <!-- Select 2 -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
  <!-- Bootstrap select -->
  <script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>

  <!-- Sweet Alert -->
  <script src="<?=base_url()?>assets/js/sweetalert.min.js"></script>

  <script type="text/javascript">
  function _max(numbers) {
    var max = 0;
    Object.keys(numbers).forEach(function (key) {
      if (numbers[key] > max) {
        max = numbers[key];
      }
    });
    return max;
  }

  var _header = $('header');
  var _content = $('#main-content');
  var _sidebar = $('#sidebar');
  var _footer = $('footer');
  _content.css('min-height', $(window).height() /*- _header.outerHeight()*/ - _footer.outerHeight());

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('a.sidebar-link[href="<?=current_url()?>"]').closest('li.sidebar-item').addClass('active');
    $('a.submenu-link[href="<?=current_url()?>"]').closest('li.submenu-item').addClass('active').closest('ul.submenu').addClass('active submenu-open').closest('li.sidebar-item').addClass('active');
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('[data-dismiss="modal"]').click(function(){
      var thisEl = $(this);
      thisEl.closest('.modal').modal('hide');
    });

    $('#btn-changepass').click(function() {
      var href = $(this).attr('href');
      $('.modal-content', $('#modal-changepass')).load(href, function() {
        $('#modal-changepass').modal('show');
        $('form', $('#modal-changepass')).validate({
          submitHandler: function(form) {
            var btnSubmit = $('button[type=submit]', $(form));
            var txtSubmit = btnSubmit[0].innerHTML;
            btnSubmit.html('<i class="far fa-circle-notch fa-spin"></i>');
            $(form).ajaxSubmit({
              url: href,
              dataType: 'json',
              type : 'post',
              success: function(res) {
                if(res.error != 0) {
                  toastr.error(res.error);
                } else {
                  toastr.success(res.success);
                  $('#modal-changepass').modal('hide');
                }
              },
              error: function() {
                toastr.error('SERVER ERROR');
              },
              complete: function() {
                btnSubmit.html(txtSubmit);
              }
            });
            return false;
          }
        });
      });
      return false;
    });
  });
  </script>
</body>
</html>
