<?php
$rperiode = $this->db
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline card-secondary">
      <div class="card-body">
        <div class="d-flex justify-content-end mb-3">
          <a href="<?=site_url('sakipv2/pemda/ajax-form-periode/add')?>" class="btn btn-primary btn-sm btn-add-periode font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</a>
        </div>
        <?php
        if(!empty($rperiode)) {
          ?>
          <div class="accordion" id="accordionPmd">
            <?php
            $first=true;
            foreach($rperiode as $r) {
              $rmisi = $this->db
              ->where(COL_IDPMD, $r[COL_PMDID])
              ->order_by(COL_MISINO, 'asc')
              ->get(TBL_SAKIPV2_PEMDA_MISI)
              ->result_array();

              $rtujuan = $this->db
              ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
              ->where(COL_IDPMD, $r[COL_PMDID])
              ->order_by(COL_TUJUANNO, 'asc')
              ->get(TBL_SAKIPV2_PEMDA_TUJUAN)
              ->result_array();

              $rsasaran = $this->db
              ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
              ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
              ->where(COL_IDPMD, $r[COL_PMDID])
              ->order_by(COL_SASARANNO, 'asc')
              ->get(TBL_SAKIPV2_PEMDA_SASARAN)
              ->result_array();
              ?>
              <div class="accordion-item">
                <h2 class="accordion-header" id="heading<?=$r[COL_PMDID]?>">
                  <button class="accordion-button fw-bold <?=$r[COL_PMDISAKTIF]==1?'':'collapsed'?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?=$r[COL_PMDID]?>">
                  <?=$r[COL_PMDTAHUNMULAI].' - '.$r[COL_PMDTAHUNAKHIR]?><?=$r[COL_PMDISAKTIF]==1?'&nbsp;<span class="badge bg-success">Aktif</span>':'&nbsp;<span class="badge bg-danger">Inaktif</span>'?>
                  </button>
                </h2>
                <div id="collapse<?=$r[COL_PMDID]?>" class="accordion-collapse <?=$r[COL_PMDISAKTIF]==1?'show':'collapse'?>"<?=$r[COL_PMDID]?> data-bs-parent="#accordionPmd" style="">
                  <div class="accordion-body">
                    <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                        <span>Kepala Daerah</span>
                        <span class="text-primary fw-bold"><?=$r[COL_PMDPEJABAT]?></span>
                      </li>
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                        <span>Misi</span>
                        <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rmisi))?></span>
                      </li>
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                        <span>Tujuan</span>
                        <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rtujuan))?></span>
                      </li>
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                        <span>Sasaran</span>
                        <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rsasaran))?></span>
                      </li>
                    </ul>
                    <div class="d-flex justify-content-end">
                      <div class="btn-group mt-3" role="group">
                        <a href="<?=site_url('sakipv2/pemda/ajax-form-periode/edit/'.$r[COL_PMDID])?>" class="btn btn-primary btn-edit-periode"><i class="far fa-edit"></i></a>
                        <?php
                        if($r[COL_PMDISAKTIF]==1) {
                          ?>
                          <button type="button" class="btn btn-secondary" disabled><i class="far fa-check-circle"></i></button>
                          <?php
                        } else {
                          ?>
                          <a href="<?=site_url('sakipv2/pemda/ajax-change-periode/activate/'.$r[COL_PMDID])?>" data-prompt="Mengaktifkan data periode pemerintahan akan otomatis me-nonaktifkan data periode pemerintahan lainnya." class="btn btn-success btn-change-periode"><i class="far fa-check-circle"></i></a>
                          <?php
                        }
                        ?>
                        <a href="<?=site_url('sakipv2/pemda/ajax-change-periode/delete/'.$r[COL_PMDID])?>" data-prompt="Menghapus data periode pemerintahan akan otomatis menghapus data RPMJD yang sudah ada." class="btn btn-danger btn-change-periode"><i class="far fa-times-circle"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$r[COL_PMDID]?>" class="btn btn-success"><i class="far fa-search"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              $first=false;
            }
            ?>
          </div>
          <?php
        } else {
          ?>
          <p class="text-center font-italic mb-0">
            BELUM ADA DATA TERSEDIA
          </p>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormPeriode" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form Periode Pemerintahan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormPeriode = $('#modalFormPeriode');
$(document).ready(function() {
  modalFormPeriode.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormPeriode).empty();
  });

  $('.btn-add-periode, .btn-edit-periode').click(function() {
    var url = $(this).attr('href');
    modalFormPeriode.modal('show');
    $('.modal-body', modalFormPeriode).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormPeriode).load(url, function(){
      $('button[type=submit]', modalFormPeriode).unbind('click').click(function(){
        $('form', modalFormPeriode).submit();
      });
    });
    return false;
  });

  $('.btn-change-periode').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
