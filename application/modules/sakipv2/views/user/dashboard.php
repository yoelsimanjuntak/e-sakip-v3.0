<?php
$ruser = GetLoggedUser();
$rskpd = array();
if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGUEST) {
  $rskpd = $this->db->where(COL_SKPDID, $ruser[COL_SKPDID])->get(TBL_SAKIPV2_SKPD)->row_array();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rmisi = array();
$rtujuan = array();
$rsasaran = array();
if(!empty($rpemda)) {
  $rmisi = $this->db
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_MISINO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_MISI)
  ->result_array();

  $rtujuan = $this->db
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_TUJUANNO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_TUJUAN)
  ->result_array();

  $rsasaran = $this->db
  ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_SASARANNO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_SASARAN)
  ->result_array();
}
?>
<section class="row">
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGUEST) {
    $rbidang = array();
    $rsubbidang = array();
    $rskpd = $this->db
    ->where(COL_SKPDISAKTIF, 1)
    ->order_by(COL_SKPDNAMA, 'asc')
    ->get(TBL_SAKIPV2_SKPD)
    ->result_array();

    if(!empty($rpemda)) {
      $rbidang = $this->db
      ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
      ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
      ->where(COL_BIDISAKTIF, 1)
      ->order_by(COL_BIDNAMA, 'asc')
      ->get(TBL_SAKIPV2_BID)
      ->result_array();

      $rsubbidang = $this->db
      ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
      ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
      ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
      ->where(COL_SUBBIDISAKTIF, 1)
      ->order_by(COL_SUBBIDNAMA, 'asc')
      ->get(TBL_SAKIPV2_SUBBID)
      ->result_array();
    }

    ?>
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            <h5><?=$this->setting_org_name?></h5>
            <p class="card-text mb-0 text-sm">
              <?php
              if(!empty($rpemda)) {
                echo $rpemda[COL_PMDVISI];
              } else {
                ?>
                Belum ada data tersedia.
                <?php
              }
              ?>
            </p>

            <?php
            if(!empty($rpemda)) {
              ?>
              <p class="card-text font-extrabold">
                <?=$rpemda[COL_PMDPEJABAT]?>
              </p>
              <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <span>Periode</span>
                  <span class="ms-1"><strong><?=$rpemda[COL_PMDTAHUNMULAI]?></strong> s.d <strong><?=$rpemda[COL_PMDTAHUNAKHIR]?></strong></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <span>Misi</span>
                  <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rmisi))?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <span>Tujuan</span>
                  <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rtujuan))?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <span>Sasaran</span>
                  <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rsasaran))?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <span>SKPD</span>
                  <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rskpd))?></span>
                </li>
              </ul>
              <!--<div class="mt-4" style="text-align: right">
                <a class="btn btn-primary" href="<?=site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rpemda[COL_PMDID]?>">Rincian <i class="far fa-arrow-circle-right"></i></a>
              </div>-->
              <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <?php
  } else {
    $rrenstra = array();
    $rdpa = array();
    $rprogram = array();
    $rkegiatan = array();
    $rsubkeg = array();
    $rtujuanskpd = array();
    $rsasaranskpd = array();
    $riku = array();
    if(!empty($rpemda)) {
      $rrenstra = $this->db
      ->where(COL_IDSKPD, $ruser[COL_SKPDID])
      ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
      ->where(COL_RENSTRAISAKTIF, 1)
      ->order_by(COL_RENSTRATAHUN, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();

      if(!empty($rrenstra)) {
        if(!empty($rrenstra[COL_RENSTRAIKU])) {
          $riku = json_decode($rrenstra[COL_RENSTRAIKU]);
        }

        $rtujuanskpd = $this->db
        ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
        ->order_by(COL_TUJUANNO, 'asc')
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
        ->result_array();

        $rsasaranskpd = $this->db
        ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
        ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
        ->order_by(COL_TUJUANNO, 'asc')
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
        ->result_array();

        $rdpa = $this->db
        ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
        ->where(COL_DPAISAKTIF, 1)
        ->order_by(COL_DPATAHUN, 'desc')
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
        ->row_array();
        if(!empty($rdpa)) {
          $rprogram = $this->db
          ->where(COL_IDDPA, $rdpa[COL_DPAID])
          ->get(TBL_SAKIPV2_BID_PROGRAM)
          ->result_array();

          $rkegiatan = $this->db
          ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
          ->where(COL_IDDPA, $rdpa[COL_DPAID])
          ->get(TBL_SAKIPV2_BID_KEGIATAN)
          ->result_array();

          $rsubkeg = $this->db
          ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
          ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
          ->where(COL_IDDPA, $rdpa[COL_DPAID])
          ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
          ->result_array();
        }
      }
    }
    ?>
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            <h5 class="mb-0"><?=!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-'?></h5>
            <?php
            if(!empty($rskpd)) {
              ?>
              <p class="card-text mb-0 text-sm"><?=!empty($rskpd[COL_SKPDNAMAPIMPINAN])?$rskpd[COL_SKPDNAMAPIMPINAN]:'-'?></p>
              <?php
            }
            ?>
            <div class="row mt-3">
              <div class="col-lg-6 col-12">
                <ul class="list-group mb-3">
                  <li class="list-group-item">
                    <p class="mb-0" style="text-align: center"><?=!empty($rrenstra)?'<strong class="text-primary">'.$rrenstra[COL_RENSTRAURAIAN].'</strong>':'<span class="text-danger">RENSTRA BELUM TERSEDIA</span>'?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>Tujuan</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rtujuanskpd))?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>Sasaran</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rsasaranskpd))?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>IKU</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($riku))?></span>
                  </li>
                  <?php
                  if(!empty($rrenstra)) {
                    ?>
                    <li class="list-group-item d-flex" style="justify-content: right">
                      <a class="btn btn-sm btn-primary" href="<?=site_url('sakipv2/skpd/index')?>">Rincian <i class="far fa-arrow-circle-right"></i></a>
                    </li>
                    <?php
                  }
                  ?>

                </ul>
              </div>
              <div class="col-lg-6 col-12">
                <ul class="list-group mb-3">
                  <li class="list-group-item">
                    <p class="mb-0" style="text-align: center"><?=!empty($rdpa)?'<strong class="text-primary">'.$rdpa[COL_DPAURAIAN].'</strong>':'<span class="text-danger">DPA BELUM TERSEDIA</span>'?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>Program</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rprogram))?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>Kegiatan</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rkegiatan))?></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>Sub Kegiatan</span>
                    <span class="badge bg-primary badge-pill badge-round ms-1"><?=number_format(count($rsubkeg))?></span>
                  </li>
                  <?php
                  if(!empty($rdpa)) {
                    ?>
                    <li class="list-group-item d-flex" style="justify-content: right">
                      <a class="btn btn-sm btn-primary" href="<?=site_url('sakipv2/skpd/renja')?>">Rincian <i class="far fa-arrow-circle-right"></i></a>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
  ?>

</section>
