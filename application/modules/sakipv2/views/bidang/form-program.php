<?php
$rOptSasaranSKPD = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDRENSTRA, !empty($def[COL_IDRENSTRA])?$def[COL_IDRENSTRA]:$data[COL_IDRENSTRA])
->order_by(COL_SASARANNO, 'asc')
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();
?>
<form id="form-program" action="<?=current_url()?>">
  <input type="hidden" name="<?=COL_IDBID?>" value="<?=!empty($def[COL_IDBID])?$def[COL_IDBID]:(!empty($data[COL_IDBID])?$data[COL_IDBID]:'')?>" />
  <input type="hidden" name="<?=COL_IDDPA?>" value="<?=!empty($def[COL_IDDPA])?$def[COL_IDDPA]:(!empty($data[COL_IDDPA])?$data[COL_IDDPA]:'')?>" />
  <?php
  if(!empty($def[COL_IDSASARANSKPD])) {
    ?>
    <input type="hidden" name="<?=COL_IDSASARANSKPD?>" value="<?=!empty($def[COL_IDSASARANSKPD])?$def[COL_IDSASARANSKPD]:(!empty($data[COL_IDSASARANSKPD])?$data[COL_IDSASARANSKPD]:'')?>" />
    <?php
  } else {
    ?>
    <div class="form-group">
      <label>SASARAN SKPD</label>
      <div class="row">
        <div class="col-lg-12">
          <select class="form-control" name="<?=COL_IDSASARANSKPD?>" style="width: 100% !important">
            <?php
            foreach($rOptSasaranSKPD as $opt) {
              ?>
              <option value="<?=$opt[COL_SASARANID]?>" <?=(!empty($data[COL_SASARANID])&&$data[COL_SASARANID]==$opt[COL_SASARANID]?'selected':'')?>>
                <?=$opt[COL_SASARANURAIAN]?>
              </option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>
    </div>

    <?php
  }
  ?>

  <div class="form-group">
    <label>KODE</label>
    <div class="row">
      <div class="col-lg-5">
        <input type="text" class="form-control" name="<?=COL_PROGRAMKODE?>" placeholder="KODE NOMENKLATUR PROGRAM" value="<?=!empty($data)?$data[COL_PROGRAMKODE]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>NOMENKLATUR</label>
    <textarea class="form-control" name="<?=COL_PROGRAMURAIAN?>" placeholder="NOMENKLATUR PROGRAM" required><?=!empty($data)?$data[COL_PROGRAMURAIAN]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-program').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
