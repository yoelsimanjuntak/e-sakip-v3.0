<?php
$ruser = GetLoggedUser();

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = '';
$getRenstra = '';
$getDPA = '';
$isDPAPerubahan = '';

if(!empty($_GET['idSKPD']) || !empty($idSkpd)) $getSkpd = !empty($_GET['idSKPD']) ? $_GET['idSKPD'] : $idSkpd;
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$rOptRenstra = array();
if(!empty($getSkpd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $getSkpd)
  //->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra']) || !empty($idRenstra)) $getRenstra = !empty($_GET['idRenstra']) ? $_GET['idRenstra'] : $idRenstra;
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rOptDpa = array();
if(!empty($getRenstra)) {
  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();
}

$getDPA = null;
if(!empty($_GET['idDPA']) || !empty($idDPA)) {
  $getDPA = !empty($_GET['idDPA']) ? $_GET['idDPA'] : $idDPA;
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

if(!empty($_GET['isDPAPerubahan'])) {
  $isDPAPerubahan = $_GET['isDPAPerubahan'];
}

$rSasaran = array();
if(!empty($getSkpd) && !empty($getRenstra)) {
  $rSasaran = $this->db
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_IDRENSTRA, $getRenstra)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}

$rimports = $this->db
->where(COL_IDSKPD, $getSkpd)
->where(COL_IDRENSTRA, $getRenstra)
->where(COL_IDDPA, $getDPA)
->get(TBL_SAKIPV2_SKPD_IMPORTS)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <form id="form-upload" action="<?=site_url('sakipv2/import/form/'.$getSkpd.'/'.$getRenstra.'/'.$getDPA)?>" method="post" enctype="multipart/form-data">
              <table class="table table-bordered mb-0">
                <thead>
                  <tr>
                    <td>
                      <div class="form-group row mb-0">
                        <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                        <div class="col-lg-10">
                          <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                            <?=$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].' '.strtoupper($rpemda[COL_PMDPEJABAT]).($rpemda['PmdIsPenjabat']==1?' (Pj.)':'')?>
                          </p>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">SKPD :</label>
                        <div class="col-lg-10">
                          <?php
                          if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                            ?>
                            <select class="form-control" name="filterSkpd">
                              <?php
                              foreach($rOptSkpd as $opt) {
                                $isSelected = '';
                                if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                  $isSelected='selected';
                                }
                                ?>
                                <option value="<?=site_url('sakipv2/import/form').'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                  <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                                </option>
                                <?php
                              }
                              ?>
                            </select>
                            <?php
                          } else {
                            $ropd = $this->db
                            ->where(COL_SKPDID, $ruser[COL_SKPDID])
                            ->get(TBL_SAKIPV2_SKPD)
                            ->row_array();
                            ?>
                            <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                              <?=strtoupper($ropd[COL_SKPDNAMA])?>
                            </p>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">RENSTRA SKPD :</label>
                        <div class="col-lg-10">
                          <select class="form-control" name="filterRenstra">
                            <?php
                            foreach($rOptRenstra as $opt) {
                              $isSelected = '';
                              if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                                $isSelected='selected';
                              }
                              ?>
                              <option value="<?=site_url('sakipv2/import/form').'?idSKPD='.$getSkpd.'&idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                                <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">DPA SKPD :</label>
                        <div class="col-lg-10">
                          <select class="form-control" name="filterDPA">
                            <?php
                            foreach($rOptDpa as $opt) {
                              ?>
                              <option value="<?=site_url('sakipv2/import/form').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                                <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">UNGGAH FILE :</label>
                        <div class="col-lg-10">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                              <label class="custom-file-label" for="file">PILIH FILE</label>
                            </div>
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-success">SUBMIT <i class="far fa-arrow-circle-right"></i></button>
                            </div>
                          </div>
                          <p class="text-sm text-muted mb-0 font-italic">
                            <strong>CATATAN:</strong><br />
                            - Format file yang diunggah adalah format yang bersumber dari <strong>Sistem Informasi Pemerintahan Daerah (SIPD)</strong><br />
                            - Jenis file / dokumen yang diperbolehkan hanya dalam format spreadsheet <strong>XLS</strong> / <strong>XLSX</strong>
                          </p>
                        </div>
                      </div>
                    </td>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>FILE</th>
                  <th style="width: 10px; white-space: nowrap">OLEH</th>
                  <th style="width: 10px; white-space: nowrap">WAKTU</th>
                  <th style="width: 10px; white-space: nowrap">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rimports)) {
                  $no=1;
                  foreach($rimports as $r) {
                    ?>
                    <tr>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=$no?></td>
                      <td>
                        <a href="<?=MY_UPLOADURL.$r[COL_IMPORTPATH]?>" target="_blank"><?=$r[COL_IMPORTPATH]?></a>
                      </td>
                      <td style="width: 10px; white-space: nowrap"><?=$r[COL_CREATEDBY]?></td>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))?></td>
                      <td class="text-center" style="width: 10px; white-space: nowrap">
                        <a href="<?=site_url('sakipv2/import/load/'.$r[COL_UNIQ])?>" class="btn btn-sm btn-outline-primary"><i class="far fa-sync"></i></a>
                      </td>
                    </tr>
                    <?php
                    $no++;
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="5" class="font-italic text-center">Belum ada data tersedia.</td>
                  </tr>
                  <?php
                }

                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterDPA],input[name=isDPAPerubahan]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  var submitUrl = $('#form-upload').attr('action');
  $('#form-upload').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              //location.reload();
              location.href = submitUrl;
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
