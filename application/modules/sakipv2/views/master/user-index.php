<?php
$ruser = GetLoggedUser();
?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline card-secondary">
      <div class="card-header">
        <div class="card-tools text-center" style="float: none !important">
          <a href="<?=site_url('sakipv2/master/user-add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH DATA</a>
          <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
        </div>
      </div>
      <div class="card-body">
        <form id="dataform" method="post" action="#">
          <table id="datalist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="text-center" style="width: 10px">AKSI</th>
                <th>USERNAME</th>
                <th>NAMA</th>
                <!--<th>ROLE</th>-->
                <th>UNIT KERJA</th>
                <th>STATUS</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form Pengguna</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modalForm');
$(document).ready(function(){
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
  });

  $('button[type=submit]', modalForm).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', modalForm).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        dt.DataTable().ajax.reload();
        if(data.error==0) {

        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        modalForm.modal('hide');
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('sakipv2/master/user-load')?>",
      "type": 'POST'
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [],
    "columnDefs": [
      {"targets":[0,4], "className":'nowrap text-center'},
      //{"targets":[3], "className":'nowrap'},
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      //{"orderable": false},
      {"orderable": false,"width": "50px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', $(row)).click(function() {
        var href = $(this).attr('href');

        $('.modal-body', modalForm).html('<p class="text-center">MEMUAT...</p>');
        modalForm.modal('show');
        $('.modal-body', modalForm).load(href, function(){
          $("select", modalForm).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add').click(function() {
    var url = $(this).attr('href');
    modalForm.modal('show');
    $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalForm).load(url, function(){
      $("select", modalForm).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });
});
</script>
