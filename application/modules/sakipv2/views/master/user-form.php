<?php

?>
<form id="form-user" action="<?=current_url()?>" method="post">
  <div class="form-group">
    <label>Nama Lengkap</label>
    <input type="text" name="<?=COL_NAME?>" class="form-control" value="<?=!empty($data)?$data[COL_NAME]:''?>" required />
  </div>
  <div class="form-group">
    <label>Username / Email</label>
    <input type="text" name="<?=COL_EMAIL?>" class="form-control" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" required />
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" name="<?=COL_PASSWORD?>" class="form-control" />
    <?=!empty($data)?'<small id="info-password" class="text-muted font-italic">Diisi hanya jika ingin mengubah password</small>':''?>
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select name="<?=COL_ROLEID?>" class="form-control" style="width: 100% !important" required>
      <option value="<?=ROLEADMIN?>" <?=!empty($data)&&$data[COL_ROLEID]==ROLEADMIN?'selected':''?>>Administrator</option>
      <option value="<?=ROLEKADIS?>" <?=!empty($data)&&$data[COL_ROLEID]==ROLEKADIS?'selected':''?>>Operator</option>
    </select>
  </div>
  <div class="form-group" id="div-opd">
    <label>OPD</label>
    <select class="form-control" name="IdentityNo" style="width: 100%">
      <?=GetCombobox("select * from sakipv2_skpd where SkpdIsAktif=1 order by SkpdNama asc", COL_SKPDID, COL_SKPDNAMA, (!empty($idUnit)?$idUnit:null))?>
    </select>
  </div>
  <!--<div class="form-group text-right mt-3">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
  </div>-->
</form>
<script type="text/javascript">
$(document).ready(function() {
  $("select", $('#form-user')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $("[name=RoleID]", $('#form-user')).change(function(){
    var _role = $("[name=RoleID]", $('#form-user')).val();
    if(_role==<?=ROLEADMIN?> || _role==<?=ROLEAPIP?>) {
      $("div#div-opd").hide();
    } else {
      $("div#div-opd").show();
    }
  }).trigger('change');

});
</script>
