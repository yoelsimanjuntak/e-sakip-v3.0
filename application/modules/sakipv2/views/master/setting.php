<?php
$ruser = GetLoggedUser();
?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline card-secondary">
      <div class="card-body">
        <?=form_open_multipart(site_url('sakipv2/master/setting'), array('role'=>'form','id'=>'form-setting-web','class'=>'form-horizontal'))?>
        <h5 class="card-title mb-4">Pengaturan Sistem</h5>
        <div class="form-group">
          <div class="row">
            <div class="col-sm-10">
              <label>Nama Aplikasi</label>
              <input type="text" class="form-control" name="SETTING_WEB_NAME" value="<?=GetSetting('SETTING_WEB_NAME')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> />
            </div>
            <div class="col-sm-2">
              <label>Versi</label>
              <input type="text" class="form-control" name="SETTING_WEB_VERSION" value="<?=GetSetting('SETTING_WEB_VERSION')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> />
            </div>
          </div>
        </div>
        <div class="form-group mt-4">
          <label>Deskripsi</label>
          <textarea class="form-control" name="SETTING_WEB_DESC" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?>><?=GetSetting('SETTING_WEB_DESC')?></textarea>
        </div>
        <h5 class="card-title mb-4 mt-4">Pengaturan Instansi</h5>
        <div class="form-group">
          <label>Nama Organisasi</label>
          <input type="text" class="form-control" name="SETTING_ORG_NAME" value="<?=GetSetting('SETTING_ORG_NAME')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> />
        </div>
        <div class="form-group">
          <label>Alamat</label>
          <textarea class="form-control" name="SETTING_ORG_ADDRESS" required  <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?>><?=GetSetting('SETTING_ORG_ADDRESS')?></textarea>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label>Telepon</label>
              <input type="text" class="form-control" name="SETTING_ORG_PHONE" value="<?=GetSetting('SETTING_ORG_PHONE')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> placeholder="Telepon" />
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>Fax</label>
              <input type="text" class="form-control" name="SETTING_ORG_FAX" value="<?=GetSetting('SETTING_ORG_FAX')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> placeholder="Fax" />
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>Email</label>
              <input type="text" class="form-control" name="SETTING_ORG_MAIL" value="<?=GetSetting('SETTING_ORG_MAIL')?>" required <?=$ruser[COL_ROLEID]==ROLEADMIN?'':'disabled'?> placeholder="Email" />
            </div>
          </div>
        </div>
        <div class="form-group mt-3">
          <button type="submit" class="btn btn-primary">SIMPAN&nbsp;<i class="far fa-arrow-circle-right"></i></button>
        </div>

        <?=form_close()?>
      </div>
    </div>
  </div>
</div>
