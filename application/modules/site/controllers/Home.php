<?php
class Home extends MY_Controller {

  function index() {
    $data['title'] = 'Beranda';
		$this->template->set('title', 'Home');
    $this->load->view('home/index-new');
  }

  function index_old() {
    $data['title'] = 'Beranda';
		$this->template->set('title', 'Home');
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function faq() {
    $data['title'] = 'Pengaduan dan Tanya Jawab';
    if(!empty($_POST)) {
      $res = $this->db->insert(TBL__FAQS, array(
        COL_NMNAMA=>$this->input->post(COL_NMNAMA),
        COL_NMKONTAK=>$this->input->post(COL_NMKONTAK),
        COL_NMKETERANGAN=>$this->input->post(COL_NMKETERANGAN),
        COL_TIMESTAMP=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      ShowJsonSuccess('Terimakasih, pengaduan / pertanyaan anda telah kami terima.');
    } else {
      $this->template->load('frontend' , 'home/faq', $data);
    }
  }

  public function status_opd($Tahun=0) {
      $data['title'] = 'Rekapitulasi Data OPD';
      $Tahun = $Tahun!=0?$Tahun:date('Y');
      $KdPemda = 0;

      $rpemda = $this->db
      ->where(COL_KD_TAHUN_FROM." <= ", date('Y'))
      ->where(COL_KD_TAHUN_TO." >= ", date('Y'))
      ->get(TBL_SAKIP_MPEMDA)
      ->row_array();

      if(!empty($rpemda)) {
        $KdPemda = $rpemda[COL_KD_PEMDA];
      }

      $data['Tahun'] = $Tahun;
      $data['KdPemda'] = $KdPemda;

      if(!empty($_GET)) {
        $kdUrusan = $this->input->get(COL_KD_URUSAN);
        $kdBidang = $this->input->get(COL_KD_BIDANG);
        $kdUnit = $this->input->get(COL_KD_UNIT);
        $kdSub = $this->input->get(COL_KD_SUB);
        $data['kdUrusan'] = $kdUrusan;
        $data['kdBidang'] = $kdBidang;
        $data['kdUnit'] = $kdUnit;
        $data['kdSub'] = $kdSub;

        $q = @"
select
opd.*,
(
	select count(*) from `sakip_mopd_tujuan` tujuan
	where
		tujuan.Kd_Pemda = $KdPemda
		and tujuan.Kd_Urusan = opd.Kd_Urusan
		and tujuan.Kd_Bidang = opd.Kd_Bidang
		and tujuan.Kd_Unit = opd.Kd_Unit
		and tujuan.Kd_Sub = opd.Kd_Sub
) as Tujuan,
(
	select count(*) from `sakip_mopd_sasaran` sasaran
	where
		sasaran.Kd_Pemda = $KdPemda
		and sasaran.Kd_Urusan = opd.Kd_Urusan
		and sasaran.Kd_Bidang = opd.Kd_Bidang
		and sasaran.Kd_Unit = opd.Kd_Unit
		and sasaran.Kd_Sub = opd.Kd_Sub
) as Sasaran
from ref_sub_unit opd
where
  opd.Kd_Urusan = $kdUrusan
  and opd.Kd_Bidang = $kdBidang
  and opd.Kd_Unit = $kdUnit
  and opd.Kd_Sub = $kdSub
        ";
        $data['res'] = $res = $this->db->query($q)->row_array();
        if(empty($res)) {
          redirect('site/home/status-opd');
        }

        $data['title'] = 'Rekapitulasi '.$res[COL_NM_SUB_UNIT];
        $this->template->load('frontend' , 'home/status_opd_detail', $data);
      } else {
        $q = @"
select
opd.*,
(
	select count(*) from `sakip_mopd_tujuan` tujuan
	where
		tujuan.Kd_Pemda = $KdPemda
		and tujuan.Kd_Urusan = opd.Kd_Urusan
		and tujuan.Kd_Bidang = opd.Kd_Bidang
		and tujuan.Kd_Unit = opd.Kd_Unit
		and tujuan.Kd_Sub = opd.Kd_Sub
) as Tujuan,
(
	select count(*) from `sakip_mopd_sasaran` sasaran
	where
		sasaran.Kd_Pemda = $KdPemda
		and sasaran.Kd_Urusan = opd.Kd_Urusan
		and sasaran.Kd_Bidang = opd.Kd_Bidang
		and sasaran.Kd_Unit = opd.Kd_Unit
		and sasaran.Kd_Sub = opd.Kd_Sub
) as Sasaran,
(
	select count(*) from `sakip_mbid_program` prg
	where
		prg.Kd_Pemda = $KdPemda
		and prg.Kd_Tahun = $Tahun
		and prg.Kd_Urusan = opd.Kd_Urusan
		and prg.Kd_Bidang = opd.Kd_Bidang
		and prg.Kd_Unit = opd.Kd_Unit
		and prg.Kd_Sub = opd.Kd_Sub
) as Program,
(
	select count(*) from `sakip_msubbid_kegiatan` keg
	where
		keg.Kd_Pemda = $KdPemda
		and keg.Kd_Tahun = $Tahun
		and keg.Kd_Urusan = opd.Kd_Urusan
		and keg.Kd_Bidang = opd.Kd_Bidang
		and keg.Kd_Unit = opd.Kd_Unit
		and keg.Kd_Sub = opd.Kd_Sub
) as Kegiatan,
(
	select count(*) from `sakip_mbid_sasaran` sasaranbid
	where
		sasaranbid.Kd_Pemda = $KdPemda
		and sasaranbid.Kd_Urusan = opd.Kd_Urusan
		and sasaranbid.Kd_Bidang = opd.Kd_Bidang
		and sasaranbid.Kd_Unit = opd.Kd_Unit
		and sasaranbid.Kd_Sub = opd.Kd_Sub
) as Cascading_Es3,
(
	select count(*) from `sakip_msubbid_sasaran` sasaransub
	where
		sasaransub.Kd_Pemda = $KdPemda
		and sasaransub.Kd_Urusan = opd.Kd_Urusan
		and sasaransub.Kd_Bidang = opd.Kd_Bidang
		and sasaransub.Kd_Unit = opd.Kd_Unit
		and sasaransub.Kd_Sub = opd.Kd_Sub
) as Cascading_Es4,
(
	select count(*) from `sakip_dpa_program` dpaprog
	where
		dpaprog.Kd_Pemda = $KdPemda
		and dpaprog.Kd_Tahun = $Tahun
		and dpaprog.Kd_Urusan = opd.Kd_Urusan
		and dpaprog.Kd_Bidang = opd.Kd_Bidang
		and dpaprog.Kd_Unit = opd.Kd_Unit
		and dpaprog.Kd_Sub = opd.Kd_Sub
) as DPA_Prg,
(
	select count(*) from `sakip_dpa_kegiatan` dpakeg
	where
		dpakeg.Kd_Pemda = $KdPemda
		and dpakeg.Kd_Tahun = $Tahun
		and dpakeg.Kd_Urusan = opd.Kd_Urusan
		and dpakeg.Kd_Bidang = opd.Kd_Bidang
		and dpakeg.Kd_Unit = opd.Kd_Unit
		and dpakeg.Kd_Sub = opd.Kd_Sub
) as DPA_Keg
from ref_sub_unit opd
order by opd.Kd_Urusan, opd.Kd_Unit, opd.Kd_Bidang, opd.Kd_Sub, opd.Nm_Sub_Unit asc
        ";
        $data['res'] = $this->db->query($q)->result_array();
        $this->template->load('frontend' , 'home/status_opd', $data);
      }
  }

  public function info_jabatan() {
    $data['title'] = 'Informasi Jabatan';
    $this->template->load('frontend' , 'home/info_jabatan', $data);
  }

  public function info_jabatan_partial() {
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : null;
    $kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : null;
    $kdSubBidang_ = $this->input->post("KdSubBidang");
    $Tahun = $this->input->post(COL_TAHUN);

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/1250 as Pegawai,
    ajbk_jabatan_bezetting.Jlh_Pegawai as Bezetting,
    ajbk_jabatan_bezetting.Tahun
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);

    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT.'.'.COL_ISAKTIF." = 1"
    ,"inner");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");
    $this->db->join(TBL_AJBK_JABATAN_BEZETTING,
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN.".".COL_KD_JABATAN." AND ".
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_TAHUN." = ".(!empty($Tahun)?$Tahun:date('Y'))
    ,"left");
    $data['res'] = $this->db
    ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')
    ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->group_by('ajbk_jabatan.Kd_Jabatan')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();
    $this->load->view('site/home/info_jabatan_', $data);
  }

  public function cetak_jabatan($id) {
    $rdata = $dat['data'] = $this->db
    ->select('*, COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as NM_JAB, edu.Opt_Name as NM_EDU')
    ->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left")
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left")
    ->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left")
    ->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left")
    ->join(TBL_AJBK_OPTION.' edu','edu.'.COL_UNIQ." = ".TBL_AJBK_JABATAN.".".COL_KD_PENDIDIKAN,"left")
    ->where(array(COL_KD_JABATAN=>$id))
    ->get(TBL_AJBK_JABATAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan');
      return;
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('ajbk/jabatan/cetak', $dat, TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle('Informasi Jabatan - '.$rdata["NM_JAB"]);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Informasi Jabatan - '.$rdata["NM_JAB"].'.pdf', 'I');
  }

  public function jabatan($id) {
    $rdata = $dat['data'] = $this->db
    ->select('*, COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as NM_JAB, edu.Opt_Name as NM_EDU, _userinformation.Name as NmVerified')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_AJBK_JABATAN.".".COL_VERIFIED_BY,"left")
    ->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left")
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left")
    ->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left")
    ->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left")
    ->join(TBL_AJBK_OPTION.' edu','edu.'.COL_UNIQ." = ".TBL_AJBK_JABATAN.".".COL_KD_PENDIDIKAN,"left")
    ->where(array(COL_KD_JABATAN=>$id))
    ->get(TBL_AJBK_JABATAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan');
      return;
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('ajbk/jabatan/cetak', $dat, TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle('Informasi Jabatan - '.$rdata["NM_JAB"]);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->SetWatermarkImage(MY_IMAGEURL.$this->setting_web_logo, 0.1, array(100,100));
    $mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Informasi Jabatan - '.$rdata["NM_JAB"].'.pdf', 'I');
  }
}
 ?>
