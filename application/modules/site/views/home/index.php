<style>
.todo-list>li:hover {
    background-color: #ccc;
}

.list-misi li {
  list-style: none !important;
  padding-left: 0 !important;
}

.list-misi li:before {
    content: "\f058";
    font-family: 'Font Awesome 5 Pro';
    font-weight: bold;
    display: inline-block;
    margin-left: -2em;
    width: 2em;
    color: #42a2b9 !important;
}
.info-box .info-box-footer {
    color: rgba(255,255,255,.8);
    display: block;
    padding: 3px 0;
    position: relative;
    text-align: center;
    text-decoration: none;
    z-index: 10;
}
.info-box .info-box-footer:hover {
  background: rgba(0,0,0,.15);
  color: #fff;
}
#div-profil::after {
  background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>dots.png) no-repeat scroll center center / 75% auto;
  content: "";
  height: 100%;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: -1;
}
</style>
<?php
$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
?>
<div class="content-header">
    <!--<div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
  <div class="container">
    <div class="row align-items-stretch">
      <div class="col-lg-8 d-flex align-items-stretch">
        <?php
        if(!empty($rpemda)) {
          $rmisi = $this->db
          ->where(COL_IDPMD, $rpemda[COL_PMDID])
          ->order_by(COL_MISINO)
          ->get(TBL_SAKIPV2_PEMDA_MISI)
          ->result_array();
          ?>
          <div class="card card-info">
            <div class="card-header">
              <h6 class="font-weight-bold text-center mb-0">
                PEMERINTAH DAERAH KOTA TEBING TINGGI<br />
                <?=$rpemda[COL_PMDTAHUNMULAI]?> s.d <?=$rpemda[COL_PMDTAHUNAKHIR]?>
              </h6>
            </div>
            <div class="card-body">
              <p class="text-center" style="line-height: 1 !important">
                <img src="<?=MY_IMAGEURL.'kepala-daerah.png'?>" class="mb-1" style="height: 20vh !important" /><br />
                <!--<strong><?=$rpemda[COL_PMDPEJABAT]?> <br /> <?=$rpemda[COL_PMDPEJABATWAKIL]?></strong>-->
                <strong><span class="text-underline">MUHAMMAD DIMIYATHI, S.SOS, M.T.P.</span><br />PJ. WALI KOTA TEBING TINGGI</strong>
              </p>

              <h6 class="text-center" style="text-decoration: underline">VISI</h6>
              <p class="text-center font-italic font-weight-bold"><?=$rpemda[COL_PMDVISI]?></p>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-12 col-sm-12">
                  <?php
                  if(!empty($rmisi)) {
                    ?>
                    <h6 style="text-decoration: underline">MISI</h6>
                    <ul class="list-misi">
                    <?php
                    $no=1;
                    foreach($rmisi as $m) {
                      ?>
                      <li class="font-weight-bold"><?=$m[COL_MISIURAIAN]?></li>
                      <?php
                      $no++;
                    }
                    ?>
                    </ul>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
          <?php
        } else {
          ?>
          <div class="card card-info">
            <div class="card-header">
              <h6 class="font-weight-bold text-center mb-0">
                SELAMAT DATANG
              </h6>
            </div>
            <div class="card-body">
              <p class="text-center mb-0" style="line-height: 1 !important">
                <strong><?=$this->setting_web_name?></strong><br /><br />
                <?=strtoupper($this->setting_web_desc)?>
              </p>
            </div>
          </div>
          <?php
        }
        ?>

      </div>
      <div class="col-lg-4 d-flex align-items-stretch">
        <div class="row">
          <div class="col-lg-12">
            <div class="info-box p-0" style="flex-wrap: wrap">
              <div class="d-flex p-2" style="flex-wrap: wrap">
                <span class="info-box-icon bg-success elevation-1"><i class="far fa-building"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text" style="text-decoration: underline">SAKIP</span>
                  <span class="info-box-number font-weight-light font-italic">
                    Sistem Akuntabilitas Kinerja Pemerintah Berbasis Elektronik
                  </span>
                </div>
                <div class="break"></div>
              </div>
              <div class="row m-0 mt-2" style="flex-basis: 100%;">
                <div class="col-12 p-0 bg-success" style="border-top: 1px solid #ddd; border-bottom-right-radius: .25rem; border-bottom-left-radius: .25rem">
                  <a href="<?=site_url('sakipv2/user/login')?>" class="info-box-footer">MASUK&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
            <div class="info-box p-0" style="flex-wrap: wrap">
              <div class="d-flex p-2" style="flex-wrap: wrap">
                <span class="info-box-icon bg-indigo elevation-1"><i class="far fa-ballot-check"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text" style="text-decoration: underline">REFORMASI BIROKRASI</span>
                  <span class="info-box-number font-weight-light font-italic">
                    Manajemen Percepatan Pelaksanaan Reformasi Birokrasi
                  </span>
                </div>
                <div class="break"></div>
              </div>
              <div class="row m-0 mt-2" style="flex-basis: 100%;">
                <div class="col-12 p-0 bg-indigo" style="border-top: 1px solid #ddd; border-bottom-right-radius: .25rem; border-bottom-left-radius: .25rem">
                  <a href="<?=site_url('rb/user/login')?>" class="info-box-footer">MASUK&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
            <div class="info-box p-0" style="flex-wrap: wrap">
              <div class="d-flex p-2" style="flex-wrap: wrap">
                <span class="info-box-icon bg-primary elevation-1"><i class="far fa-sitemap"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text" style="text-decoration: underline">ANJAB & ABK</span>
                  <span class="info-box-number font-weight-light font-italic">
                    Sistem Informasi Analisis Jabatan dan Analisis Beban Kerja
                  </span>
                </div>
                <div class="break"></div>
              </div>

              <div class="row m-0 mt-2" style="flex-basis: 100%;">
                <div class="col-12 p-0 bg-primary" style="border-top: 1px solid #ddd; border-bottom-right-radius: .25rem; border-bottom-left-radius: .25rem">
                  <a href="<?=site_url('ajbk/user/login')?>" class="info-box-footer">MASUK&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
            <div class="info-box p-0" style="flex-wrap: wrap">
              <div class="d-flex p-2" style="flex-wrap: wrap">
                <span class="info-box-icon bg-danger elevation-1"><i class="far fa-cogs"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text" style="text-decoration: underline">CONTROL PANEL</span>
                  <span class="info-box-number font-weight-light font-italic">
                    Panel Kontrol Administrator SITALAKBAJAKUN
                  </span>
                </div>
                <div class="break"></div>
              </div>

              <div class="row m-0 mt-2" style="flex-basis: 100%;">
                <div class="col-12 p-0 bg-danger" style="border-top: 1px solid #ddd; border-bottom-right-radius: .25rem; border-bottom-left-radius: .25rem">
                  <a href="<?=site_url('site/user/login')?>" class="info-box-footer">MASUK&nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
