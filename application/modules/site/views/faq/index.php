<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      (!empty($d[COL_TIMESTAMP])?date('d M Y H:i', strtotime($d[COL_TIMESTAMP])):"-"),
      $d[COL_NMNAMA],
      $d[COL_NMKONTAK],
      $d[COL_NMKETERANGAN]
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small>Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active">Keluhan / Aduan</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-default">
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
              "autoWidth" : false,
              //"sDom": "Rlfrtip",
              "aaData": <?=$data?>,
              //"bJQueryUI": true,
              //"aaSorting" : [[5,'desc']],
              "scrollY" : '40vh',
              "scrollX": "200%",
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
              "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
              "order": [[ 0, "desc" ]],
              "aoColumns": [
                  {"sTitle": "Tanggal / Waktu",},
                  {"sTitle": "Nama"},
                  {"sTitle": "Kontak"},
                  {"sTitle": "Isi"},
                  //{"sTitle": "Address"},
                  //{"sTitle": "Last Login", "width": "15%"}
              ]
            });
        });
    </script>
