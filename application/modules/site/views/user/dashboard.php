<?php
$ropd = $this->db->where(COL_SKPDISAKTIF, 1)->get(TBL_SAKIPV2_SKPD)->result_array();
$ruser = $this->db->get(TBL__USERS)->result_array();
$rfaq = $this->db->get(TBL__FAQS)->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard v2</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard v2</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format(count($ropd))?></h3>
            <p>SKPD</p>
          </div>
          <div class="icon">
            <i class="fas fa-university"></i>
          </div>
          <a href="<?=site_url('site/master/opd-index')?>" class="small-box-footer">LIHAT SEMUA <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h3><?=number_format(count($ruser))?></h3>
            <p>Pengguna</p>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="<?=site_url('site/user/index')?>" class="small-box-footer">LIHAT SEMUA <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?=number_format(count($rfaq))?></h3>
            <p>Keluhan / Aduan</p>
          </div>
          <div class="icon">
            <i class="fas fa-comments"></i>
          </div>
          <a href="<?=site_url('site/faq/index')?>" class="small-box-footer">LIHAT SEMUA <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
