# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: lkt_esakip
# Generation Time: 2024-04-03 01:44:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_faqs`;

CREATE TABLE `_faqs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NmNama` varchar(200) DEFAULT NULL,
  `NmKontak` varchar(200) DEFAULT NULL,
  `NmKeterangan` text,
  `Timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2023-12-25 21:13:24','http://localhost/karo-esakip/sakipv2/subbidang/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(2,'2023-12-25 21:13:26','http://localhost/karo-esakip/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(3,'2023-12-25 21:13:33','http://localhost/karo-esakip/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(4,'2023-12-25 21:49:28','http://localhost/karo-esakip/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(5,'2023-12-25 21:49:28','http://localhost/karo-esakip/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(6,'2023-12-25 21:50:13','http://localhost/karo-esakip/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
	(7,'2024-03-13 21:23:46','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(8,'2024-03-13 21:24:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(9,'2024-03-13 21:24:08','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(10,'2024-03-13 21:24:10','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(11,'2024-03-13 21:24:12','http://localhost/esakip-langkat/sakipv2/bidang/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(12,'2024-03-13 21:24:13','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(13,'2024-03-13 21:24:20','http://localhost/esakip-langkat/sakipv2/pemda/monev.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(14,'2024-03-13 21:25:08','http://localhost/esakip-langkat/sakipv2/pemda/monev.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(15,'2024-03-13 21:42:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(16,'2024-03-13 21:47:38','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(17,'2024-03-13 21:47:38','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(18,'2024-03-13 21:52:22','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(19,'2024-03-13 21:53:16','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(20,'2024-03-13 21:53:41','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(21,'2024-03-13 21:53:52','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(22,'2024-03-13 21:53:58','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(23,'2024-03-13 21:54:11','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(24,'2024-03-13 21:54:31','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(25,'2024-03-13 21:54:52','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(26,'2024-03-13 21:55:39','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(27,'2024-03-13 22:03:58','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(28,'2024-03-13 22:03:58','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(29,'2024-03-13 22:04:02','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(30,'2024-03-13 22:04:02','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(31,'2024-03-13 22:04:10','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(32,'2024-03-13 22:06:23','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(33,'2024-03-13 22:13:28','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(34,'2024-03-13 22:13:28','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(35,'2024-03-13 22:13:38','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(36,'2024-03-13 22:13:38','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(37,'2024-03-13 22:15:54','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(38,'2024-03-13 22:16:04','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(39,'2024-03-13 22:16:04','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(40,'2024-03-13 22:17:18','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(41,'2024-03-13 22:18:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(42,'2024-03-13 22:18:49','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(43,'2024-03-13 22:19:29','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(44,'2024-03-13 22:22:32','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(45,'2024-03-13 22:23:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(46,'2024-03-13 22:23:48','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(47,'2024-03-13 22:25:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(48,'2024-03-13 22:27:18','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(49,'2024-03-13 22:27:54','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(50,'2024-03-13 22:27:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(51,'2024-03-13 22:40:50','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(52,'2024-03-13 22:42:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(53,'2024-03-13 22:42:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(54,'2024-03-13 22:42:45','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(55,'2024-03-13 22:43:14','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(56,'2024-03-13 22:43:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(57,'2024-03-13 22:44:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(58,'2024-03-13 22:45:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(59,'2024-03-13 22:45:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(60,'2024-03-13 22:45:50','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(61,'2024-03-13 22:46:03','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(62,'2024-03-13 22:46:08','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(63,'2024-03-13 22:46:22','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(64,'2024-03-13 22:46:38','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(65,'2024-03-13 22:46:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(66,'2024-03-13 22:47:01','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(67,'2024-03-13 22:47:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(68,'2024-03-13 22:47:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(69,'2024-03-13 22:47:30','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(70,'2024-03-13 22:47:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(71,'2024-03-13 22:47:55','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(72,'2024-03-13 22:48:42','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(73,'2024-03-13 22:48:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(74,'2024-03-13 22:49:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(75,'2024-03-13 22:49:05','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(76,'2024-03-13 22:49:47','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(77,'2024-03-13 22:49:50','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(78,'2024-03-13 22:50:30','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(79,'2024-03-13 22:50:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(80,'2024-03-13 22:50:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(81,'2024-03-13 22:50:55','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(82,'2024-03-13 22:51:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(83,'2024-03-14 05:41:33','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(84,'2024-03-14 05:41:51','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(85,'2024-03-14 05:41:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(86,'2024-03-14 05:44:05','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(87,'2024-03-14 05:47:08','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(88,'2024-03-14 05:47:35','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(89,'2024-03-14 05:47:37','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(90,'2024-03-14 05:48:10','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(91,'2024-03-14 05:49:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(92,'2024-03-14 05:49:52','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(93,'2024-03-14 05:50:18','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(94,'2024-03-14 05:50:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(95,'2024-03-14 05:51:14','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(96,'2024-03-14 05:52:04','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(97,'2024-03-14 05:54:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(98,'2024-03-14 05:56:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(99,'2024-03-14 05:57:37','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(100,'2024-03-14 05:58:32','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(101,'2024-03-14 06:02:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(102,'2024-03-14 06:04:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(103,'2024-03-14 06:04:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(104,'2024-03-14 06:05:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(105,'2024-03-14 06:05:34','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(106,'2024-03-14 06:05:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(107,'2024-03-14 06:10:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(108,'2024-03-14 06:10:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(109,'2024-03-14 06:11:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(110,'2024-03-14 06:12:00','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(111,'2024-03-14 06:13:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(112,'2024-03-14 06:14:16','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(113,'2024-03-14 06:17:14','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(114,'2024-03-14 06:18:29','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(115,'2024-03-14 06:21:00','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(116,'2024-03-14 06:21:34','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(117,'2024-03-14 06:21:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(118,'2024-03-14 06:23:48','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(119,'2024-03-14 06:24:09','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(120,'2024-03-14 06:24:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(121,'2024-03-14 06:24:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(122,'2024-03-14 06:25:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(123,'2024-03-14 06:25:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(124,'2024-03-14 06:25:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(125,'2024-03-14 06:27:36','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(126,'2024-03-14 06:30:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(127,'2024-03-14 06:30:18','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(128,'2024-03-14 06:31:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(129,'2024-03-14 06:31:54','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(130,'2024-03-14 06:32:31','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(131,'2024-03-14 06:33:01','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(132,'2024-03-14 06:33:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(133,'2024-03-14 06:34:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(134,'2024-03-14 06:34:30','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(135,'2024-03-14 06:34:58','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(136,'2024-03-14 06:35:04','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(137,'2024-03-14 06:35:14','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(138,'2024-03-14 06:35:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(139,'2024-03-14 06:35:34','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(140,'2024-03-14 06:35:56','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(141,'2024-03-14 06:36:11','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(142,'2024-03-14 06:36:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(143,'2024-03-14 06:36:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(144,'2024-03-14 06:36:41','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(145,'2024-03-14 06:37:03','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(146,'2024-03-14 06:37:10','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(147,'2024-03-14 06:37:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(148,'2024-03-14 06:38:59','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(149,'2024-03-14 06:48:32','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(150,'2024-03-14 06:54:28','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(151,'2024-03-14 07:06:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(152,'2024-03-14 07:06:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(153,'2024-03-14 07:07:01','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(154,'2024-03-14 07:07:07','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(155,'2024-03-14 07:07:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(156,'2024-03-14 07:07:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(157,'2024-03-14 07:14:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(158,'2024-03-14 07:14:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(159,'2024-03-14 07:16:48','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(160,'2024-03-14 07:16:52','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(161,'2024-03-14 08:54:48','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(162,'2024-03-14 10:17:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(163,'2024-03-14 10:17:56','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(164,'2024-03-14 10:20:22','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(165,'2024-03-14 10:21:47','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(166,'2024-03-14 10:24:40','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(167,'2024-03-14 10:25:16','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(168,'2024-03-14 10:25:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(169,'2024-03-14 10:25:45','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(170,'2024-03-14 10:27:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(171,'2024-03-14 10:27:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(172,'2024-03-14 10:27:43','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(173,'2024-03-14 10:28:22','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(174,'2024-03-14 10:30:54','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(175,'2024-03-14 10:33:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(176,'2024-03-14 10:34:01','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(177,'2024-03-14 10:34:39','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(178,'2024-03-14 10:35:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(179,'2024-03-14 10:35:12','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(180,'2024-03-14 10:35:18','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(181,'2024-03-14 10:35:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(182,'2024-03-14 10:35:29','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(183,'2024-03-14 10:35:47','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(184,'2024-03-14 10:36:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(185,'2024-03-14 10:36:14','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(186,'2024-03-14 10:36:28','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(187,'2024-03-14 10:36:46','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(188,'2024-03-14 10:36:49','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(189,'2024-03-14 10:37:53','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(190,'2024-03-14 10:37:58','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(191,'2024-03-14 10:38:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(192,'2024-03-14 10:38:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(193,'2024-03-14 10:38:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(194,'2024-03-14 10:38:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(195,'2024-03-14 10:39:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(196,'2024-03-14 10:39:47','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(197,'2024-03-14 10:41:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(198,'2024-03-14 10:41:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(199,'2024-03-14 10:41:46','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(200,'2024-03-14 10:43:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(201,'2024-03-14 10:44:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(202,'2024-03-14 10:44:40','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(203,'2024-03-14 10:44:47','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(204,'2024-03-14 10:44:54','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(205,'2024-03-14 10:45:28','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(206,'2024-03-14 10:48:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(207,'2024-03-14 10:51:06','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(208,'2024-03-14 10:51:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(209,'2024-03-14 10:53:17','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(210,'2024-03-14 10:53:37','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(211,'2024-03-14 10:53:48','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(212,'2024-03-14 10:53:55','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(213,'2024-03-14 10:54:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(214,'2024-03-14 10:55:41','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(215,'2024-03-14 10:55:50','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(216,'2024-03-14 10:56:07','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(217,'2024-03-14 11:02:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(218,'2024-03-14 11:02:46','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(219,'2024-03-14 11:03:14','http://localhost/esakip-langkat/sakipv2/user/changepassword.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(220,'2024-03-14 11:03:16','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(221,'2024-03-14 11:03:29','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(222,'2024-03-14 11:03:30','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(223,'2024-03-14 11:03:30','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(224,'2024-03-14 11:04:06','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(225,'2024-03-14 11:04:07','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(226,'2024-03-14 11:04:31','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(227,'2024-03-14 11:04:31','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(228,'2024-03-14 11:04:51','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(229,'2024-03-14 11:04:53','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(230,'2024-03-14 11:05:07','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(231,'2024-03-14 11:05:15','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(232,'2024-03-14 11:05:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(233,'2024-03-14 11:06:13','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(234,'2024-03-14 11:06:28','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(235,'2024-03-14 11:06:30','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(236,'2024-03-14 11:06:30','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(237,'2024-03-14 11:06:32','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(238,'2024-03-14 11:08:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(239,'2024-03-14 11:13:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(240,'2024-03-14 11:13:55','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(241,'2024-03-14 11:14:27','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(242,'2024-03-14 11:15:57','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(243,'2024-03-14 11:16:06','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(244,'2024-03-14 11:16:53','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(245,'2024-03-14 11:17:26','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(246,'2024-03-14 11:17:30','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(247,'2024-03-14 11:17:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(248,'2024-03-14 11:18:16','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(249,'2024-03-14 11:18:52','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(250,'2024-03-14 11:19:07','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(251,'2024-03-14 11:19:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(252,'2024-03-14 11:19:33','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(253,'2024-03-14 11:19:39','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(254,'2024-03-14 11:19:47','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(255,'2024-03-14 11:21:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(256,'2024-03-14 11:21:35','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(257,'2024-03-14 11:22:04','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Mobile Safari/537.36'),
	(258,'2024-03-14 11:22:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(259,'2024-03-14 11:23:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(260,'2024-03-14 11:24:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(261,'2024-03-14 11:24:10','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(262,'2024-03-14 11:24:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(263,'2024-03-14 11:24:35','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(264,'2024-03-14 11:25:09','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(265,'2024-03-14 11:26:29','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(266,'2024-03-14 11:26:32','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(267,'2024-03-14 11:27:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(268,'2024-03-14 11:27:34','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(269,'2024-03-14 11:29:32','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(270,'2024-03-14 11:29:32','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(271,'2024-03-14 11:30:01','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(272,'2024-03-14 11:30:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'),
	(273,'2024-04-02 18:35:01','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(274,'2024-04-02 18:35:01','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(275,'2024-04-02 18:35:12','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(276,'2024-04-02 18:35:12','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(277,'2024-04-02 18:35:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(278,'2024-04-02 18:35:39','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(279,'2024-04-02 18:37:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(280,'2024-04-02 18:47:44','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(281,'2024-04-02 21:19:14','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(282,'2024-04-02 21:19:14','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(283,'2024-04-02 21:19:19','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(284,'2024-04-02 21:19:19','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(285,'2024-04-02 21:23:49','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(286,'2024-04-02 21:24:06','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(287,'2024-04-02 21:24:58','http://localhost/esakip-langkat/sakipv2/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(288,'2024-04-02 21:24:58','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(289,'2024-04-02 21:25:04','http://localhost/esakip-langkat/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(290,'2024-04-02 21:25:05','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(291,'2024-04-02 21:25:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(292,'2024-04-02 21:25:45','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(293,'2024-04-02 21:25:53','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(294,'2024-04-02 21:26:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(295,'2024-04-02 21:26:29','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(296,'2024-04-02 21:26:49','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(297,'2024-04-02 21:28:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(298,'2024-04-02 21:29:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(299,'2024-04-02 21:29:58','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(300,'2024-04-02 21:30:25','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(301,'2024-04-02 21:30:27','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(302,'2024-04-02 21:30:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(303,'2024-04-02 21:31:23','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(304,'2024-04-02 21:31:24','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(305,'2024-04-02 21:31:41','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(306,'2024-04-02 21:32:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(307,'2024-04-02 21:32:59','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(308,'2024-04-02 21:33:48','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(309,'2024-04-02 21:34:05','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(310,'2024-04-02 21:35:43','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(311,'2024-04-02 21:38:27','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(312,'2024-04-02 21:38:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(313,'2024-04-02 21:38:46','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(314,'2024-04-02 21:42:34','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(315,'2024-04-02 21:42:42','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(316,'2024-04-02 21:44:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(317,'2024-04-02 21:44:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(318,'2024-04-02 21:44:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(319,'2024-04-02 21:45:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(320,'2024-04-02 21:45:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(321,'2024-04-02 21:47:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(322,'2024-04-02 21:49:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(323,'2024-04-02 21:49:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(324,'2024-04-02 21:50:06','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(325,'2024-04-02 21:50:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(326,'2024-04-02 21:51:15','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(327,'2024-04-02 21:51:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(328,'2024-04-02 21:51:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(329,'2024-04-02 21:51:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(330,'2024-04-02 21:54:18','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(331,'2024-04-02 21:54:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(332,'2024-04-02 21:54:40','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(333,'2024-04-02 21:54:47','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(334,'2024-04-02 21:54:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(335,'2024-04-02 21:55:15','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(336,'2024-04-02 21:55:34','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(337,'2024-04-02 21:56:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(338,'2024-04-02 21:56:23','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(339,'2024-04-02 21:57:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(340,'2024-04-02 21:58:13','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(341,'2024-04-02 21:59:17','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(342,'2024-04-02 21:59:37','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(343,'2024-04-02 21:59:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(344,'2024-04-02 22:00:32','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(345,'2024-04-02 22:00:39','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(346,'2024-04-02 22:00:42','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(347,'2024-04-02 22:01:06','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(348,'2024-04-02 22:01:36','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(349,'2024-04-02 22:01:42','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(350,'2024-04-02 22:01:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(351,'2024-04-02 22:02:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(352,'2024-04-02 22:02:08','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(353,'2024-04-02 22:02:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(354,'2024-04-02 22:03:56','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(355,'2024-04-02 22:04:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(356,'2024-04-02 22:06:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(357,'2024-04-02 22:06:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(358,'2024-04-02 22:06:49','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(359,'2024-04-02 22:07:08','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(360,'2024-04-02 22:07:14','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(361,'2024-04-02 22:07:19','http://localhost/esakip-langkat/sakipv2/pemda/ajax-change-periode/activate/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(362,'2024-04-02 22:07:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(363,'2024-04-02 22:08:16','http://localhost/esakip-langkat/sakipv2/pemda/ajax-change-periode/activate/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(364,'2024-04-02 22:08:21','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(365,'2024-04-02 22:09:01','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(366,'2024-04-02 22:09:08','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(367,'2024-04-02 22:09:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(368,'2024-04-02 22:11:09','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(369,'2024-04-02 22:11:20','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(370,'2024-04-02 22:11:42','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(371,'2024-04-02 22:11:58','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(372,'2024-04-02 22:12:16','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(373,'2024-04-02 22:12:24','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(374,'2024-04-02 22:13:13','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(375,'2024-04-02 22:13:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(376,'2024-04-02 22:13:24','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(377,'2024-04-02 22:13:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(378,'2024-04-02 22:13:50','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(379,'2024-04-02 22:14:02','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(380,'2024-04-02 22:15:09','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(381,'2024-04-02 22:15:17','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(382,'2024-04-02 22:15:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(383,'2024-04-02 22:15:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(384,'2024-04-02 22:16:34','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(385,'2024-04-02 22:16:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(386,'2024-04-02 22:16:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(387,'2024-04-02 22:17:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(388,'2024-04-02 22:17:10','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(389,'2024-04-02 22:17:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(390,'2024-04-02 22:19:06','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(391,'2024-04-02 22:19:56','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(392,'2024-04-02 22:20:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(393,'2024-04-02 22:29:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(394,'2024-04-02 22:29:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(395,'2024-04-02 22:29:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(396,'2024-04-02 22:30:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(397,'2024-04-02 22:30:25','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(398,'2024-04-02 22:30:42','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(399,'2024-04-02 22:30:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(400,'2024-04-02 22:31:02','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(401,'2024-04-02 22:31:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(402,'2024-04-02 22:33:04','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(403,'2024-04-02 22:33:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(404,'2024-04-02 22:33:20','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(405,'2024-04-02 22:35:08','http://localhost/esakip-langkat/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(406,'2024-04-02 22:35:09','http://localhost/esakip-langkat/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(407,'2024-04-02 22:35:26','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(408,'2024-04-02 22:36:13','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(409,'2024-04-02 22:36:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(410,'2024-04-02 22:36:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(411,'2024-04-02 22:36:53','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(412,'2024-04-02 22:37:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(413,'2024-04-02 22:37:01','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(414,'2024-04-02 22:37:04','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(415,'2024-04-02 22:37:11','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(416,'2024-04-02 22:37:12','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(417,'2024-04-02 22:37:38','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(418,'2024-04-02 22:38:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(419,'2024-04-02 22:42:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(420,'2024-04-02 22:44:24','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(421,'2024-04-02 22:45:59','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(422,'2024-04-02 22:47:10','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(423,'2024-04-02 22:47:20','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(424,'2024-04-02 22:47:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(425,'2024-04-02 22:48:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(426,'2024-04-02 22:48:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(427,'2024-04-02 22:49:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(428,'2024-04-02 22:49:45','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(429,'2024-04-02 22:51:21','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(430,'2024-04-02 22:51:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(431,'2024-04-02 22:52:39','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(432,'2024-04-02 22:52:58','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(433,'2024-04-02 22:53:05','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(434,'2024-04-02 22:54:01','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(435,'2024-04-02 22:54:41','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(436,'2024-04-02 22:57:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(437,'2024-04-02 22:58:19','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(438,'2024-04-02 22:58:29','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(439,'2024-04-02 22:59:41','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(440,'2024-04-02 22:59:48','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(441,'2024-04-02 23:00:09','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(442,'2024-04-02 23:00:39','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(443,'2024-04-02 23:00:46','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(444,'2024-04-02 23:00:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(445,'2024-04-02 23:00:59','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(446,'2024-04-02 23:01:04','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(447,'2024-04-02 23:01:13','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(448,'2024-04-02 23:01:15','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(449,'2024-04-02 23:01:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(450,'2024-04-02 23:01:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(451,'2024-04-02 23:02:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(452,'2024-04-02 23:02:02','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(453,'2024-04-02 23:02:14','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(454,'2024-04-02 23:02:19','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(455,'2024-04-02 23:02:20','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(456,'2024-04-02 23:02:23','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(457,'2024-04-02 23:02:27','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(458,'2024-04-02 23:02:53','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(459,'2024-04-02 23:03:03','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(460,'2024-04-02 23:03:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(461,'2024-04-02 23:03:10','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(462,'2024-04-02 23:03:12','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(463,'2024-04-02 23:03:20','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(464,'2024-04-02 23:03:30','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(465,'2024-04-02 23:03:38','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(466,'2024-04-02 23:04:18','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(467,'2024-04-02 23:04:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(468,'2024-04-02 23:04:25','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(469,'2024-04-02 23:04:28','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(470,'2024-04-02 23:04:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(471,'2024-04-02 23:04:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(472,'2024-04-02 23:04:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(473,'2024-04-02 23:05:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(474,'2024-04-02 23:06:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(475,'2024-04-02 23:06:55','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(476,'2024-04-02 23:07:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(477,'2024-04-02 23:07:36','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(478,'2024-04-02 23:08:06','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(479,'2024-04-02 23:08:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(480,'2024-04-02 23:10:10','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(481,'2024-04-02 23:14:30','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(482,'2024-04-02 23:14:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(483,'2024-04-02 23:15:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(484,'2024-04-02 23:15:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(485,'2024-04-02 23:15:38','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(486,'2024-04-02 23:15:49','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(487,'2024-04-02 23:15:58','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(488,'2024-04-02 23:16:12','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(489,'2024-04-02 23:16:18','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(490,'2024-04-02 23:17:37','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(491,'2024-04-02 23:17:47','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(492,'2024-04-02 23:18:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(493,'2024-04-02 23:18:14','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(494,'2024-04-02 23:18:19','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(495,'2024-04-02 23:18:43','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(496,'2024-04-02 23:18:48','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(497,'2024-04-02 23:19:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(498,'2024-04-02 23:19:36','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(499,'2024-04-02 23:20:06','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(500,'2024-04-02 23:20:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(501,'2024-04-02 23:20:21','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(502,'2024-04-02 23:20:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(503,'2024-04-02 23:20:41','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(504,'2024-04-02 23:20:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(505,'2024-04-02 23:20:53','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(506,'2024-04-02 23:21:05','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(507,'2024-04-02 23:21:10','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(508,'2024-04-02 23:23:36','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(509,'2024-04-02 23:24:00','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(510,'2024-04-02 23:24:30','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(511,'2024-04-02 23:24:39','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(512,'2024-04-02 23:24:44','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(513,'2024-04-02 23:24:49','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(514,'2024-04-02 23:25:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(515,'2024-04-02 23:26:50','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(516,'2024-04-02 23:27:22','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(517,'2024-04-02 23:28:02','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(518,'2024-04-02 23:28:12','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(519,'2024-04-02 23:28:20','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(520,'2024-04-02 23:29:18','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(521,'2024-04-02 23:36:12','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(522,'2024-04-02 23:37:48','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(523,'2024-04-02 23:38:39','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(524,'2024-04-02 23:38:51','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(525,'2024-04-02 23:39:03','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(526,'2024-04-02 23:39:24','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(527,'2024-04-02 23:39:35','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(528,'2024-04-02 23:40:16','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(529,'2024-04-02 23:40:57','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(530,'2024-04-02 23:41:00','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(531,'2024-04-02 23:41:07','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(532,'2024-04-02 23:42:53','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(533,'2024-04-02 23:42:56','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(534,'2024-04-02 23:43:22','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(535,'2024-04-02 23:43:31','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(536,'2024-04-02 23:44:17','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(537,'2024-04-02 23:44:19','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(538,'2024-04-02 23:44:49','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(539,'2024-04-02 23:45:52','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(540,'2024-04-02 23:45:57','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(541,'2024-04-02 23:46:05','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(542,'2024-04-02 23:46:14','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(543,'2024-04-02 23:48:46','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(544,'2024-04-02 23:51:33','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(545,'2024-04-02 23:51:41','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(546,'2024-04-02 23:51:53','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(547,'2024-04-02 23:56:09','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(548,'2024-04-02 23:56:24','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(549,'2024-04-02 23:56:37','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(550,'2024-04-02 23:57:19','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(551,'2024-04-02 23:57:26','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(552,'2024-04-02 23:57:29','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(553,'2024-04-02 23:57:43','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(554,'2024-04-02 23:57:49','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(555,'2024-04-02 23:58:01','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(556,'2024-04-02 23:58:09','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(557,'2024-04-02 23:58:41','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(558,'2024-04-02 23:58:56','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(559,'2024-04-02 23:59:34','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(560,'2024-04-03 00:00:21','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(561,'2024-04-03 00:03:32','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(562,'2024-04-03 00:04:53','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(563,'2024-04-03 00:06:52','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(564,'2024-04-03 00:07:13','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(565,'2024-04-03 00:07:34','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(566,'2024-04-03 00:07:46','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(567,'2024-04-03 00:07:54','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(568,'2024-04-03 00:08:23','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(569,'2024-04-03 00:08:51','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(570,'2024-04-03 00:09:00','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(571,'2024-04-03 00:09:05','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(572,'2024-04-03 00:10:17','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(573,'2024-04-03 00:10:32','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(574,'2024-04-03 00:10:35','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(575,'2024-04-03 00:10:40','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(576,'2024-04-03 00:10:55','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(577,'2024-04-03 00:11:19','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(578,'2024-04-03 00:11:40','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(579,'2024-04-03 00:12:25','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(580,'2024-04-03 00:12:51','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(581,'2024-04-03 00:13:59','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(582,'2024-04-03 00:14:30','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(583,'2024-04-03 00:14:50','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(584,'2024-04-03 00:15:03','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(585,'2024-04-03 00:15:21','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(586,'2024-04-03 00:15:31','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(587,'2024-04-03 00:15:40','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(588,'2024-04-03 00:16:08','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(589,'2024-04-03 00:17:19','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(590,'2024-04-03 00:17:24','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(591,'2024-04-03 00:17:48','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(592,'2024-04-03 00:18:09','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(593,'2024-04-03 00:18:12','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(594,'2024-04-03 00:18:41','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(595,'2024-04-03 00:18:51','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(596,'2024-04-03 00:19:04','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(597,'2024-04-03 00:19:15','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(598,'2024-04-03 00:19:25','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(599,'2024-04-03 00:19:33','http://localhost/esakip-langkat/sakipv2/pemda/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(600,'2024-04-03 00:19:37','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(601,'2024-04-03 00:19:39','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(602,'2024-04-03 00:23:21','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(603,'2024-04-03 00:23:33','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(604,'2024-04-03 00:23:47','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(605,'2024-04-03 00:27:58','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(606,'2024-04-03 00:28:08','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(607,'2024-04-03 00:28:22','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(608,'2024-04-03 00:28:24','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(609,'2024-04-03 00:28:26','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(610,'2024-04-03 00:28:29','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(611,'2024-04-03 00:29:57','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(612,'2024-04-03 00:30:08','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(613,'2024-04-03 00:30:37','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(614,'2024-04-03 00:30:51','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(615,'2024-04-03 00:31:32','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(616,'2024-04-03 00:31:46','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(617,'2024-04-03 00:31:54','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(618,'2024-04-03 00:32:04','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(619,'2024-04-03 00:32:31','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(620,'2024-04-03 00:33:36','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(621,'2024-04-03 00:33:45','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(622,'2024-04-03 00:34:11','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(623,'2024-04-03 00:34:24','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(624,'2024-04-03 00:34:34','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(625,'2024-04-03 00:34:39','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(626,'2024-04-03 00:35:54','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(627,'2024-04-03 00:36:33','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(628,'2024-04-03 00:37:12','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(629,'2024-04-03 00:41:25','http://localhost/esakip-langkat/sakipv2/skpd/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(630,'2024-04-03 00:41:36','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(631,'2024-04-03 08:15:42','http://localhost/esakip-langkat/sakipv2/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(632,'2024-04-03 08:15:42','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(633,'2024-04-03 08:31:43','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(634,'2024-04-03 08:31:45','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'),
	(635,'2024-04-03 08:39:43','http://localhost/esakip-langkat/sakipv2/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Bappeda'),
	(3,'Operator OPD'),
	(4,'Operator Bidang OPD'),
	(5,'Operator Sub Bidang OPD'),
	(6,'Operator Keuangan'),
	(7,'Operator APIP'),
	(8,'Admin Kuisioner'),
	(99,'Guest');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','E-SAKIP'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Akuntabilitas Kinerja Instansi Pemerintah Berbasis Elektronik'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','-'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','PEMERINTAH KAB. LANGKAT'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. T. Amir Hamzah No.1 Stabat'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(061) 8910202'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','(061) 8910603'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','sekretariat@langkatkab.go.id'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','2.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'LANGKATKAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2024-04-02 21:25:04','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rb_doc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_doc`;

CREATE TABLE `rb_doc` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `MonevPeriod` int(11) NOT NULL,
  `DocName` varchar(200) DEFAULT NULL,
  `DocURL` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_mperubahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_mperubahan`;

CREATE TABLE `rb_mperubahan` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmPerubahan` text,
  `NmIndikator` text,
  `NmKegiatan` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renja`;

CREATE TABLE `rb_renja` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Tahun` int(11) NOT NULL,
  `NmType` enum('INSTANSI','UNIT') NOT NULL DEFAULT 'INSTANSI',
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmKeterangan` text,
  `SkpdId` bigint(20) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjadet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjadet`;

CREATE TABLE `rb_renjadet` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdPerubahan` bigint(20) unsigned NOT NULL,
  `NmKegiatan` text,
  `NmTahapan` text,
  `NmTarget` varchar(100) DEFAULT NULL,
  `NmSatuan` varchar(100) DEFAULT NULL,
  `NmOutput` text,
  `NmIndikator` text,
  `NmSubKegiatan` text,
  `NmPenanggungJawab` text,
  `NmPelaksana` text,
  `PeriodTarget` text,
  `TipePengukuran` varchar(100) DEFAULT NULL,
  `NumN1Target` varchar(100) DEFAULT NULL,
  `NumN1Capaian` varchar(100) DEFAULT NULL,
  `NumN2Target` varchar(100) DEFAULT NULL,
  `NumN2Capaian` varchar(100) DEFAULT NULL,
  `BudgetTW1` double DEFAULT NULL,
  `BudgetTW2` double DEFAULT NULL,
  `BudgetTW3` double DEFAULT NULL,
  `BudgetTW4` double DEFAULT NULL,
  `NmKeterkaitan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJADET_RENJA` (`IdRenja`),
  KEY `FK_RENJADET_INDIKATOR` (`IdPerubahan`),
  CONSTRAINT `FK_RENJADET_INDIKATOR` FOREIGN KEY (`IdPerubahan`) REFERENCES `rb_renjaperubahan` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RENJADET_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjamonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjamonev`;

CREATE TABLE `rb_renjamonev` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdTahapan` bigint(20) unsigned NOT NULL,
  `MonevPeriod` int(11) NOT NULL,
  `MonevKeterangan` text,
  `MonevTarget` varchar(100) DEFAULT NULL,
  `MonevCapaian` varchar(100) NOT NULL,
  `MonevEvidence` text,
  `MonevBelanja` double DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJAMONEV_RENJA` (`IdRenja`),
  KEY `FK_RENJAMONEV_TAHAPAN` (`IdTahapan`),
  CONSTRAINT `FK_RENJAMONEV_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RENJAMONEV_TAHAPAN` FOREIGN KEY (`IdTahapan`) REFERENCES `rb_renjadet` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjaperubahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjaperubahan`;

CREATE TABLE `rb_renjaperubahan` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdSkpd` bigint(20) DEFAULT NULL,
  `NmPerubahan` text,
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmIndikator` text,
  `NmKegiatan` text,
  `NmOutput` text,
  `NmTarget` varchar(100) DEFAULT NULL,
  `NmPenanggungJawab` text,
  `NmKriteria` text,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJAKEG_RENJA` (`IdRenja`),
  CONSTRAINT `FK_RENJAKEG_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ref_sub_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_sub_unit`;

CREATE TABLE `ref_sub_unit` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(20) NOT NULL,
  `Kd_Bidang` bigint(20) NOT NULL,
  `Kd_Unit` bigint(20) NOT NULL,
  `Kd_Sub` bigint(20) NOT NULL,
  `Nm_Sub_Unit` varchar(255) NOT NULL,
  `Nm_Pimpinan` varchar(255) DEFAULT NULL,
  `Nm_Kop1` varchar(255) DEFAULT NULL,
  `Nm_Kop2` varchar(255) DEFAULT NULL,
  `IsAktif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`) USING BTREE,
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sakip_msatuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msatuan`;

CREATE TABLE `sakip_msatuan` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `Kd_Satuan` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sakip_msatuan` WRITE;
/*!40000 ALTER TABLE `sakip_msatuan` DISABLE KEYS */;

INSERT INTO `sakip_msatuan` (`Uniq`, `Kd_Satuan`, `Nm_Satuan`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,'%','Persen','admin','2019-03-14 23:59:59',NULL,NULL),
	(2,'Nilai','Nilai','admin','2019-03-14 23:59:59',NULL,NULL),
	(3,'Indeks','Indeks','admin','2019-03-14 23:59:59',NULL,NULL),
	(4,'Unit','Unit','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:25'),
	(5,'ORG','Orang','admin','2019-03-14 23:59:59',NULL,NULL),
	(6,'OP','Opini','admin','2019-03-14 23:59:59',NULL,NULL),
	(7,'KEL','Kelompok','admin','2019-03-14 23:59:59',NULL,NULL),
	(8,'CABOR','Cabor','admin','2019-03-14 23:59:59',NULL,NULL),
	(9,'TIM','Tim','admin','2019-03-14 23:59:59',NULL,NULL),
	(10,'Dokumen','Dokumen','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:33'),
	(11,'Jasa','Jasa','admin','2019-03-14 23:59:59',NULL,NULL),
	(12,'Item','Item','admin','2019-03-14 23:59:59',NULL,NULL),
	(13,'Jenis','Jenis','admin','2019-03-14 23:59:59',NULL,NULL),
	(14,'Bahan Bacaan','Bahan Bacaan','admin','2019-03-14 23:59:59',NULL,NULL),
	(15,'Kebutuhan','Kebutuhan','admin','2019-03-14 23:59:59',NULL,NULL),
	(16,'Kali','Kali','admin','2019-03-14 23:59:59',NULL,NULL),
	(17,'Lembaga','Lembaga','admin','2019-03-14 23:59:59',NULL,NULL),
	(18,'Paket','Paket','admin','2019-03-14 23:59:59',NULL,NULL),
	(19,'Gugus & IGTK','Gugus & IGTK','admin','2019-03-14 23:59:59',NULL,NULL),
	(20,'Ruang','Ruang','admin','2019-03-14 23:59:59',NULL,NULL),
	(21,'KK-DATADIK PAUD','KK-DATADIK PAUD','admin','2019-03-14 23:59:59',NULL,NULL),
	(22,'LS','LS','admin','2019-03-14 23:59:59',NULL,NULL),
	(23,'Siswa','Siswa','admin','2019-03-14 23:59:59',NULL,NULL),
	(24,'Mapel','Mapel','admin','2019-03-14 23:59:59',NULL,NULL),
	(25,'Mata Lomba','Mata Lomba','admin','2019-03-14 23:59:59',NULL,NULL),
	(26,'Kegiatan','Kegiatan','admin','2019-03-14 23:59:59',NULL,NULL),
	(27,'Buku','Buku','admin','2019-03-14 23:59:59',NULL,NULL),
	(28,'Sekolah','Sekolah','admin','2019-03-14 23:59:59',NULL,NULL),
	(29,'Lokasi','Lokasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(30,'Exemplar','Exemplar','admin','2019-03-14 23:59:59',NULL,NULL),
	(31,'Set','Set','admin','2019-03-14 23:59:59',NULL,NULL),
	(32,'Event','Event','admin','2019-03-14 23:59:59',NULL,NULL),
	(33,'Keping','Keping','admin','2019-03-14 23:59:59',NULL,NULL),
	(34,'Aplikasi','Aplikasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(35,'Pagelaran','Pagelaran','admin','2019-03-14 23:59:59',NULL,NULL),
	(36,'Wadah','Wadah','admin','2019-03-14 23:59:59',NULL,NULL),
	(37,'Buah','Buah','admin','2019-03-14 23:59:59',NULL,NULL),
	(38,'Desa','Desa','admin','2019-03-14 23:59:59',NULL,NULL),
	(39,'Batas Perwilayahan','Batas Perwilayahan','admin','2019-03-14 23:59:59',NULL,NULL),
	(40,'Produk Hukum','Produk Hukum','admin','2019-03-14 23:59:59',NULL,NULL),
	(41,'WP','WP','admin','2019-03-14 23:59:59',NULL,NULL),
	(42,'Rekening','Rekening','admin','2019-03-15 08:49:13',NULL,NULL),
	(43,'OH','OH','admin','2019-03-15 08:49:23',NULL,NULL),
	(44,'Ha','Hektare','admin','2019-03-19 11:01:43','admin','2019-08-27 02:23:47'),
	(45,'Ton','Ton','admin','2019-03-19 11:01:59',NULL,NULL),
	(47,'Batang','Batang','admin','2019-03-19 11:02:30',NULL,NULL),
	(48,'Komoditi','Komoditi','admin','2019-03-19 11:02:51',NULL,NULL),
	(49,'Poktan','Poktan','admin','2019-03-19 12:15:56',NULL,NULL),
	(51,'Km','Kilometer','admin','2019-03-20 12:03:37',NULL,NULL),
	(52,'Penangkar','Penangkar','admin','2019-03-20 12:51:18',NULL,NULL),
	(53,'WTP','Wajar Tanpa Pengecualian','admin','2019-03-20 12:56:22',NULL,NULL),
	(54,'WDP','Wajar Dengan Pengecualian','admin','2019-03-20 12:56:37',NULL,NULL),
	(55,'Disclaimer','Disclaimer','admin','2019-03-20 12:57:04',NULL,NULL),
	(56,'Level','Level','admin','2019-03-20 12:57:14',NULL,NULL),
	(57,'Laporan','Laporan','admin','2019-03-20 12:57:25',NULL,NULL),
	(58,'Rencana Aksi','Rencana Aksi','admin','2019-03-20 12:57:41',NULL,NULL),
	(59,'OPD','OPD','admin','2019-03-20 12:57:59','admin','2019-08-22 09:09:23'),
	(60,'Koperasi','Koperasi','admin','2019-03-21 04:26:35',NULL,NULL),
	(61,'UMKM','UMKM','admin','2019-03-21 04:26:48',NULL,NULL),
	(62,'IKM','IKM','admin','2019-03-21 04:27:32',NULL,NULL),
	(63,'Pasar','Pasar','admin','2019-03-21 04:28:21',NULL,NULL),
	(64,'Rumah Tangga','Rumah Tangga','admin','2019-03-21 05:07:57',NULL,NULL),
	(65,'Meter','Meter','admin','2019-03-21 05:08:06',NULL,NULL),
	(66,'65','kecamatan','admin','2019-05-06 09:53:05',NULL,NULL),
	(67,'Bulan','Bulan','admin','2019-07-16 03:55:03',NULL,NULL),
	(68,'Unit Kerja','Unit Kerja','admin','2019-08-15 03:49:22',NULL,NULL),
	(69,'Perpustakaan','Perpustakaan','admin','2019-08-15 03:49:33',NULL,NULL),
	(72,'Ekor','Ekor','admin','2019-08-15 03:49:54',NULL,NULL),
	(74,'Hari','Hari','admin','2019-08-19 07:13:55',NULL,NULL),
	(75,'Bungkus','Bungkus','admin','2019-08-19 07:14:08',NULL,NULL),
	(76,'Tilang','Tilang','admin','2019-08-19 07:14:24',NULL,NULL),
	(77,'Perusahaan','Perusahaan','admin','2019-08-19 07:14:33',NULL,NULL),
	(78,'Organisasi','Organisasi','admin','2019-08-20 07:02:57',NULL,NULL),
	(79,'DI','Daerah Irigasi','admin','2019-08-20 07:03:07',NULL,NULL),
	(80,'Peserta','Peserta','admin','2019-08-20 11:05:19',NULL,NULL),
	(81,'kg','kg','admin','2019-08-21 03:43:21',NULL,NULL),
	(82,'kg/kapita/tahun','kg/kapita/tahun','admin','2019-08-21 03:46:01',NULL,NULL),
	(83,'Produk','Produk','admin','2019-08-21 04:14:14',NULL,NULL),
	(84,'Boot/Stand','Boot/Stand','admin','2019-08-21 04:15:10',NULL,NULL),
	(85,'Tepat Waktu','Tepat Waktu','admin','2019-08-21 08:07:00',NULL,NULL),
	(86,'SP2D','SP2D','admin','2019-08-21 08:07:22',NULL,NULL),
	(87,'Temuan','Temuan','admin','2019-08-21 08:07:29',NULL,NULL),
	(88,'Rp','Rupiah','admin','2019-08-21 08:07:55',NULL,NULL),
	(89,'RTP','RTP','admin','2019-08-21 09:11:11',NULL,NULL),
	(90,'Pedagang','Pedagang','admin','2019-08-21 09:29:02',NULL,NULL),
	(91,'Faskes','Fasilitas Kesehatan','admin','2019-08-21 09:36:01',NULL,NULL),
	(92,'PPKBD dan Sub PPKKBD','PPKBD dan Sub PPKKBD','admin','2019-08-21 09:55:14',NULL,NULL),
	(93,'Bangunan','Bangunan','admin','2019-08-21 10:00:22',NULL,NULL),
	(94,'Kepala Keluarga','Kepala Keluarga (KK)','admin','2019-08-21 10:04:27',NULL,NULL),
	(95,'Skor','Skor','admin','2019-08-21 11:20:46',NULL,NULL),
	(96,'Angka','Angka','admin','2019-08-21 11:20:57',NULL,NULL),
	(97,'Kampung KB','Kampung KB','admin','2019-08-21 11:32:27',NULL,NULL),
	(98,'Informasi','Informasi','admin','2019-08-22 02:41:47',NULL,NULL),
	(99,'Meter Persegi','Meter Persegi','admin','2019-08-22 05:20:17',NULL,NULL),
	(100,'KWT','KWT','admin','2019-08-22 07:56:32',NULL,NULL),
	(101,'Poktan','Poktan','admin','2019-08-22 08:13:52',NULL,NULL),
	(102,'Predikat','Predikat','admin','2019-08-22 09:09:04',NULL,NULL),
	(103,'Kasus','Kasus','admin','2019-08-27 09:29:28',NULL,NULL),
	(104,'Responden','Responden','admin','2019-08-27 10:18:45',NULL,NULL),
	(105,'Sertifikat','Sertifikat','admin','2019-08-27 11:36:15',NULL,NULL),
	(106,'Pelaku usaha dan atau kegiatan','Pelaku usaha dan atau kegiatan','admin','2019-08-27 12:52:54',NULL,NULL),
	(107,'Judul','Judul','admin','2019-08-28 00:39:02',NULL,NULL),
	(108,'Peraturan','Peraturan','admin','2019-08-28 01:09:45',NULL,NULL),
	(109,'Titik','Titik','admin','2019-08-28 04:53:05',NULL,NULL),
	(110,'Puskesmas','Puskesmas','admin','2019-08-28 04:56:10',NULL,NULL),
	(111,'Titik','Titik','admin','2019-08-28 05:27:58',NULL,NULL),
	(112,'Media Informasi','Media Informasi','admin','2019-08-28 07:43:41',NULL,NULL),
	(113,'Wajib Pajak','Wajib Pajak','admin','2019-08-28 09:50:29',NULL,NULL),
	(114,'SPPT','SPPT','admin','2019-08-28 09:51:15',NULL,NULL),
	(115,'Balai Penyuluhan','Balai Penyuluhan','admin','2019-08-28 12:08:03',NULL,NULL),
	(116,'Rumah','Rumah','admin','2019-08-29 04:37:30',NULL,NULL),
	(117,'Sampel','Sampel','admin','2019-08-30 03:05:53',NULL,NULL),
	(118,'Leaflet/baliho','Leaflet/baliho','admin','2019-08-30 03:06:12',NULL,NULL),
	(119,'Pemotong','Pemotong','admin','2019-08-30 03:24:07',NULL,NULL),
	(120,'Dosis','Dosis','admin','2019-08-30 04:25:40',NULL,NULL),
	(121,'Tahun','Tahun','admin','2020-05-26 14:23:20',NULL,NULL),
	(122,'KM / JAM','KM / JAM','admin','2021-09-03 00:00:00',NULL,NULL),
	(123,'proyek','Proyek','admin','2021-09-10 11:08:38',NULL,NULL),
	(124,'KELURAHAN','KELURAHAN','','0000-00-00 00:00:00',NULL,NULL),
	(125,'POSYANTEK','POSYANTEK','','0000-00-00 00:00:00',NULL,NULL),
	(126,'KELURAHAN','KELURAHAN','admin','2021-09-12 12:00:00',NULL,NULL),
	(127,'POSYANTEK','POSYANTEK','admin','2021-09-12 12:00:00',NULL,NULL),
	(128,'Partai','Partai','admin','2021-09-20 20:09:18',NULL,NULL),
	(129,'PSKS','PSKS','','0000-00-00 00:00:00',NULL,NULL),
	(130,'PSKS','PSKS','admin','0000-00-00 00:00:00',NULL,NULL),
	(131,'KARTU','KARTU','admin','2022-03-10 21:19:19',NULL,NULL),
	(132,'SURAT','SURAT','admin','2022-03-10 21:19:19',NULL,NULL),
	(133,'PANTI','PANTI','admin','2022-03-10 21:20:40',NULL,NULL),
	(134,'DATA','DATA','admin','2022-03-11 11:06:06',NULL,NULL),
	(135,'LAYANAN','LAYANAN','admin','2022-03-21 17:08:32',NULL,NULL),
	(136,'Kw/Ha','Kw/Ha','admin','2023-03-08 09:04:37',NULL,NULL),
	(137,'Ltr','Liter','admin','2023-03-08 09:04:37',NULL,NULL),
	(138,'Bibit','Bibit','admin','2023-03-08 09:05:12',NULL,NULL),
	(139,'Kelompok','Kelompok','admin','2023-03-08 09:05:12',NULL,NULL),
	(140,'Arsip','Arsip','admin','2023-03-08 20:15:43',NULL,NULL),
	(141,'Eksemplar','Eksemplar','admin','2023-03-08 20:15:43',NULL,NULL),
	(142,'Berita Acara','Berita Acara','admin','2023-03-08 20:16:22',NULL,NULL),
	(143,'Perangkat Daerah','Perangkat Daerah','admin','2023-03-08 20:16:22',NULL,NULL),
	(144,'Berkas','Berkas','admin','2023-03-08 20:19:07',NULL,NULL),
	(145,'Rekomendasi','Rekomendasi','admin','2023-03-08 22:46:48',NULL,NULL),
	(146,'Lembaga Kemasyarakatan','Lembaga Kemasyarakatan','admin','2023-03-08 22:46:48',NULL,NULL),
	(147,'Pokmas / Ormas','Pokmas / Ormas','admin','2023-03-08 22:49:53',NULL,NULL),
	(148,'Pokmas / Ormas','Pokmas / Ormas','admin','2023-03-08 22:49:53',NULL,NULL),
	(149,'Masukan','Masukan','','2023-03-08 23:06:09',NULL,NULL),
	(150,'Orang / Bulan','Orang / Bulan','admin','2023-03-08 23:06:38',NULL,NULL),
	(151,'Sarana','Sarana','admin','2023-03-09 00:07:06',NULL,NULL),
	(152,'Kunjungan','Kunjungan','admin','2023-03-09 00:07:06',NULL,NULL),
	(153,'Keluarga','Keluarga','admin','2023-03-09 00:07:34',NULL,NULL),
	(154,'Makam','Makam','admin','2023-03-09 00:07:34',NULL,NULL),
	(155,'Validasi','Validasi','admin','2023-03-09 00:49:43',NULL,NULL),
	(156,'Pelaku Usaha','Pelaku Usaha','admin','2023-03-09 01:35:48',NULL,NULL),
	(157,'Meter Kubik','Meter Kubik','admin','2023-03-09 02:47:45',NULL,NULL),
	(158,'Institusi','Institusi','admin','2023-03-09 02:50:06',NULL,NULL),
	(159,'Liter / Detik','Liter / Detik','admin','2023-03-09 03:15:18',NULL,NULL),
	(160,'SR','SR','admin','2023-03-09 03:15:18',NULL,NULL),
	(161,'Kawasan','Kawasan','admin','2023-03-09 03:29:33',NULL,NULL),
	(162,'SKPD','SKPD','admin','2023-03-09 19:49:44',NULL,NULL),
	(163,'Obrik','Obrik','admin','2023-03-28 23:10:27',NULL,NULL),
	(164,'Kesepakatan','Kesepakatan','admin','2023-03-29 20:27:56',NULL,NULL),
	(165,'Jabatan','Jabatan','admin','2023-03-29 21:50:50',NULL,NULL),
	(166,'Indikator Sasaran','Indikator Sasaran','admin','2023-03-29 21:53:21',NULL,NULL);

/*!40000 ALTER TABLE `sakip_msatuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_bid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid`;

CREATE TABLE `sakipv2_bid` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `BidId` bigint(20) NOT NULL AUTO_INCREMENT,
  `BidNama` varchar(200) NOT NULL DEFAULT '',
  `BidNamaPimpinan` varchar(200) DEFAULT NULL,
  `BidNamaJabatan` varchar(200) DEFAULT NULL,
  `BidIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `BidTugasPokok` text,
  `BidFungsi` text,
  `BidIKU` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`BidId`),
  KEY `FK_BID_RENSTRA` (`IdRenstra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_kegiatan`;

CREATE TABLE `sakipv2_bid_kegiatan` (
  `IdProgram` bigint(20) unsigned NOT NULL,
  `KegiatanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `KegiatanKode` varchar(200) DEFAULT NULL,
  `KegiatanUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`KegiatanId`),
  KEY `FK_KEGIATAN_PROGRAM` (`IdProgram`),
  CONSTRAINT `FK_KEGIATAN_PROGRAM` FOREIGN KEY (`IdProgram`) REFERENCES `sakipv2_bid_program` (`ProgramId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_kegsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_kegsasaran`;

CREATE TABLE `sakipv2_bid_kegsasaran` (
  `IdKegiatan` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_KEGSASARAN_KEGIATAN` (`IdKegiatan`),
  CONSTRAINT `FK_KEGSASARAN_KEGIATAN` FOREIGN KEY (`IdKegiatan`) REFERENCES `sakipv2_bid_kegiatan` (`KegiatanId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_program`;

CREATE TABLE `sakipv2_bid_program` (
  `IdBid` bigint(20) NOT NULL,
  `IdDPA` bigint(20) unsigned NOT NULL,
  `IdSasaranSkpd` bigint(20) unsigned NOT NULL,
  `ProgramId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ProgramKode` varchar(200) DEFAULT NULL,
  `ProgramUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ProgramId`),
  KEY `FK_PROGRAM_BID` (`IdBid`),
  KEY `FK_PROGRAM_DPA` (`IdDPA`),
  KEY `FK_PROGRAM_PMDSASARAN` (`IdSasaranSkpd`),
  CONSTRAINT `FK_PROGRAM_DPA` FOREIGN KEY (`IdDPA`) REFERENCES `sakipv2_skpd_renstra_dpa` (`DPAId`),
  CONSTRAINT `FK_PROGRAM_RENSTRASASARAN` FOREIGN KEY (`IdSasaranSkpd`) REFERENCES `sakipv2_skpd_renstra_sasaran` (`SasaranId`),
  CONSTRAINT `sakipv2_bid_program_ibfk_1` FOREIGN KEY (`IdBid`) REFERENCES `sakipv2_bid` (`BidId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_progsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_progsasaran`;

CREATE TABLE `sakipv2_bid_progsasaran` (
  `IdProgram` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_PROGSASARAN_PROG` (`IdProgram`),
  CONSTRAINT `FK_PROGSASARAN_PROG` FOREIGN KEY (`IdProgram`) REFERENCES `sakipv2_bid_program` (`ProgramId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda`;

CREATE TABLE `sakipv2_pemda` (
  `PmdId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PmdTahunMulai` int(11) NOT NULL,
  `PmdTahunAkhir` int(11) NOT NULL,
  `PmdPejabat` varchar(200) DEFAULT NULL,
  `PmdPejabatWakil` varchar(200) DEFAULT NULL,
  `PmdVisi` text,
  `PmdIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `PmdIsPenjabat` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PmdId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda` (`PmdId`, `PmdTahunMulai`, `PmdTahunAkhir`, `PmdPejabat`, `PmdPejabatWakil`, `PmdVisi`, `PmdIsAktif`, `PmdIsPenjabat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2024,2026,'H. M. FAISAL HASRIMY, AP, MAP','-','RENCANA PEMBANGUNAN DAERAH',1,NULL,'admin','2024-03-14 06:30:15','',NULL);

/*!40000 ALTER TABLE `sakipv2_pemda` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_pemda_misi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_misi`;

CREATE TABLE `sakipv2_pemda_misi` (
  `IdPmd` bigint(20) unsigned NOT NULL,
  `MisiId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MisiNo` int(11) NOT NULL,
  `MisiUraian` text NOT NULL,
  `MisiIKU` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`MisiId`),
  KEY `FK_MISI_PMD` (`IdPmd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda_misi` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda_misi` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda_misi` (`IdPmd`, `MisiId`, `MisiNo`, `MisiUraian`, `MisiIKU`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2,1,'Lorem Ipsum',NULL,'admin','2024-04-02 22:30:15','admin','2024-04-02 22:36:59'),
	(1,4,2,'Lorem Ipsum',NULL,'admin','2024-04-02 22:31:01','admin','2024-04-02 22:37:10');

/*!40000 ALTER TABLE `sakipv2_pemda_misi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_pemda_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasaran`;

CREATE TABLE `sakipv2_pemda_sasaran` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) NOT NULL,
  `SasaranUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_SASARAN_TUJUAN` (`IdTujuan`),
  CONSTRAINT `FK_SASARAN_TUJUAN` FOREIGN KEY (`IdTujuan`) REFERENCES `sakipv2_pemda_tujuan` (`TujuanId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda_sasaran` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda_sasaran` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda_sasaran` (`IdTujuan`, `SasaranId`, `SasaranNo`, `SasaranUraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,1,1,'Sasaran 01.01','admin','2024-04-02 23:25:34','admin','2024-04-02 23:27:21');

/*!40000 ALTER TABLE `sakipv2_pemda_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_pemda_sasarandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasarandet`;

CREATE TABLE `sakipv2_pemda_sasarandet` (
  `IdSasaran` bigint(20) unsigned NOT NULL,
  `SsrIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SsrIndikatorUraian` text,
  `SsrIndikatorSumberData` text,
  `SsrIndikatorFormulasi` text,
  `SsrIndikatorSatuan` text,
  `SsrIndikatorTarget` text,
  `SsrIndikatorRealisasi` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SsrIndikatorId`),
  KEY `FK_SASARANDET_SASARAN` (`IdSasaran`),
  CONSTRAINT `FK_SASARANDET_SASARAN` FOREIGN KEY (`IdSasaran`) REFERENCES `sakipv2_pemda_sasaran` (`SasaranId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda_sasarandet` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda_sasarandet` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda_sasarandet` (`IdSasaran`, `SsrIndikatorId`, `SsrIndikatorUraian`, `SsrIndikatorSumberData`, `SsrIndikatorFormulasi`, `SsrIndikatorSatuan`, `SsrIndikatorTarget`, `SsrIndikatorRealisasi`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2,'INDIKATOR 1.1','LOREM IPSUM','LOREM IPSUM','Persen','100',NULL,'admin','2024-04-02 23:27:21',NULL,NULL),
	(1,3,'Indikator 1.2','LOREM IPSUM','LOREM IPSUM','Persen','100',NULL,'admin','2024-04-02 23:27:21',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_pemda_sasarandet` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_pemda_sasaranmonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasaranmonev`;

CREATE TABLE `sakipv2_pemda_sasaranmonev` (
  `IdSasaranIndikator` bigint(20) unsigned DEFAULT NULL,
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MonevTahun` int(11) NOT NULL,
  `MonevTarget` text,
  `MonevTargetTW1` text,
  `MonevTargetTW2` text,
  `MonevTargetTW3` text,
  `MonevTargetTW4` text,
  `MonevRealisasi` text,
  `MonevRealisasiTW1` text,
  `MonevRealisasiTW2` text,
  `MonevRealisasiTW3` text,
  `MonevRealisasiTW4` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_MONEV_PMDSASARANDET` (`IdSasaranIndikator`),
  CONSTRAINT `FK_MONEV_PMDSASARANDET` FOREIGN KEY (`IdSasaranIndikator`) REFERENCES `sakipv2_pemda_sasarandet` (`SsrIndikatorId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_tujuan`;

CREATE TABLE `sakipv2_pemda_tujuan` (
  `IdMisi` bigint(20) unsigned NOT NULL,
  `TujuanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujuanNo` int(11) NOT NULL,
  `TujuanUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujuanId`),
  KEY `FK_TUJUAN_MISI` (`IdMisi`),
  CONSTRAINT `FK_TUJUAN_MISI` FOREIGN KEY (`IdMisi`) REFERENCES `sakipv2_pemda_misi` (`MisiId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda_tujuan` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda_tujuan` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda_tujuan` (`IdMisi`, `TujuanId`, `TujuanNo`, `TujuanUraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,2,1,'Tujuan Pemerintah Daerah','admin','2024-04-02 23:03:29',NULL,NULL),
	(2,3,2,'Tujuan Pemerintah Daerah','admin','2024-04-02 23:03:37',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_pemda_tujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_pemda_tujuandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_tujuandet`;

CREATE TABLE `sakipv2_pemda_tujuandet` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `TujIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujIndikatorUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujIndikatorId`),
  KEY `FK_TUJUANDET_TUJUAN` (`IdTujuan`),
  CONSTRAINT `FK_TUJUANDET_TUJUAN` FOREIGN KEY (`IdTujuan`) REFERENCES `sakipv2_pemda_tujuan` (`TujuanId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_pemda_tujuandet` WRITE;
/*!40000 ALTER TABLE `sakipv2_pemda_tujuandet` DISABLE KEYS */;

INSERT INTO `sakipv2_pemda_tujuandet` (`IdTujuan`, `TujIndikatorId`, `TujIndikatorUraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,1,'Indikator 1.1','admin','2024-04-02 23:15:00',NULL,NULL),
	(2,2,'Indikator 1.2','admin','2024-04-02 23:18:07',NULL,NULL),
	(3,3,'Indikator 2.1','admin','2024-04-02 23:18:14',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_pemda_tujuandet` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd`;

CREATE TABLE `sakipv2_skpd` (
  `SkpdId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SkpdNama` text,
  `SkpdNamaPimpinan` varchar(200) DEFAULT NULL,
  `SkpdNamaJabatan` varchar(200) DEFAULT NULL,
  `SkpdUrusan` bigint(20) NOT NULL,
  `SkpdBidang` bigint(20) NOT NULL,
  `SkpdUnit` bigint(20) NOT NULL,
  `SkpdSubUnit` bigint(20) NOT NULL,
  `SkpdKop` text,
  `SkpdIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SkpdId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd` (`SkpdId`, `SkpdNama`, `SkpdNamaPimpinan`, `SkpdNamaJabatan`, `SkpdUrusan`, `SkpdBidang`, `SkpdUnit`, `SkpdSubUnit`, `SkpdKop`, `SkpdIsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SEKRETARIAT DAERAH',NULL,NULL,9,1,1,1,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(2,'SEKRETARIAT DPRD',NULL,NULL,9,1,1,2,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(3,'INSPEKTORAT',NULL,NULL,9,1,1,3,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(4,'DINAS PENDIDIKAN',NULL,NULL,9,1,1,4,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(5,'DINAS KESEHATAN',NULL,NULL,9,1,1,5,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(6,'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG',NULL,NULL,9,1,1,6,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(7,'DINAS PERUMAHAN RAKYAT DAN KAWASAN PERMUKIMAN SERTA PERTANAHAN',NULL,NULL,9,1,1,7,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(8,'DINAS SOSIAL',NULL,NULL,9,1,1,8,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(9,'DINAS LINGKUNGAN HIDUP',NULL,NULL,9,1,1,9,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(10,'DINAS KETENAGAKERJAAN, KOPERASI, USAHA KECIL DAN MENENGAH ',NULL,NULL,9,1,1,10,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(11,'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL',NULL,NULL,9,1,1,11,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(12,'DINAS PEMBERDAYAAN MASYARAKAT DESA',NULL,NULL,9,1,1,12,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(13,'DINAS PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK SERTA PENGENDALIAN PENDUDUK',NULL,NULL,9,1,1,13,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(14,'DINAS PERHUBUNGAN',NULL,NULL,9,1,1,14,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(15,'DINAS KOMUNIKASI DAN INFORMATIKA',NULL,NULL,9,1,1,15,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(16,'DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU',NULL,NULL,9,1,1,16,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(17,'DINAS KEBUDAYAAN, PEMUDA DAN OLAHRAGA SERTA PARIWISATA',NULL,NULL,9,1,1,17,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(18,'DINAS PERINDUSTRIAN DAN PERDAGANGAN',NULL,NULL,9,1,1,18,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(19,'DINAS PERPUSTAKAAN DAN ARSIP',NULL,NULL,9,1,1,19,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(20,'SATUAN POLISI PAMONG PRAJA',NULL,NULL,9,1,1,20,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(21,'DINAS KETAHANAN PANGAN DAN PERIKANAN',NULL,NULL,9,1,1,21,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(22,'BADAN PERENCANAAN PEMBANGUNAN DAERAH, PENELITIAN DAN PENGEMBANGAN',NULL,NULL,9,1,1,22,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(23,'BADAN KEUANGAN DAN ASET DAERAH',NULL,NULL,9,1,1,23,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(24,'BADAN PENDAPATAN DAERAH',NULL,NULL,9,1,1,24,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(25,'BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA',NULL,NULL,9,1,1,25,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(26,'BADAN PENANGGULANGAN BENCANA DAERAH',NULL,NULL,9,1,1,26,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(27,'BADAN KESATUAN BANGSA DAN POLITIK','JOHN DOE',NULL,9,1,1,27,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL),
	(28,'RSUD',NULL,NULL,9,1,1,28,NULL,1,'admin','2025-12-23 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_doc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_doc`;

CREATE TABLE `sakipv2_skpd_doc` (
  `IdSkpd` bigint(20) unsigned DEFAULT NULL,
  `DocId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DocName` text NOT NULL,
  `DocRemarks` text,
  `DocTahun` int(11) DEFAULT NULL,
  `DocURL` text NOT NULL,
  `DocType` enum('renstra','pohon-kinerja-ip','pohon-kinerja','crosscutting','lakip','lainnya','lke-rb','ren-rb','rb-lainnya') DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`DocId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_imports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_imports`;

CREATE TABLE `sakipv2_skpd_imports` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSkpd` bigint(10) unsigned NOT NULL,
  `IdRenstra` bigint(10) unsigned NOT NULL,
  `IdDPA` bigint(10) unsigned NOT NULL,
  `ImportPath` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra`;

CREATE TABLE `sakipv2_skpd_renstra` (
  `IdSkpd` bigint(20) unsigned NOT NULL,
  `IdPemda` bigint(20) unsigned NOT NULL,
  `RenstraId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `RenstraTahun` int(11) NOT NULL,
  `RenstraUraian` text,
  `RenstraIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `RenstraTugasPokok` text,
  `RenstraFungsi` text,
  `RenstraIKU` text,
  `RenstraOrg` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`RenstraId`),
  KEY `FK_RENSTRA_SKPD` (`IdSkpd`),
  KEY `FK_RENSTRA_PMD` (`IdPemda`),
  CONSTRAINT `FK_RENSTRA_PMD` FOREIGN KEY (`IdPemda`) REFERENCES `sakipv2_pemda` (`PmdId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_RENSTRA_SKPD` FOREIGN KEY (`IdSkpd`) REFERENCES `sakipv2_skpd` (`SkpdId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd_renstra` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd_renstra` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd_renstra` (`IdSkpd`, `IdPemda`, `RenstraId`, `RenstraTahun`, `RenstraUraian`, `RenstraIsAktif`, `RenstraTugasPokok`, `RenstraFungsi`, `RenstraIKU`, `RenstraOrg`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(27,1,1,2018,'RENSTRA INDUK',1,NULL,NULL,NULL,NULL,'bakesbangpol','2024-03-14 11:04:50',NULL,NULL),
	(23,1,2,2024,'Renstra 2024 - 2026',1,NULL,NULL,NULL,NULL,'admin','2024-04-03 00:17:47',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd_renstra` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_renstra_dpa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_dpa`;

CREATE TABLE `sakipv2_skpd_renstra_dpa` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `DPAId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DPATahun` int(11) NOT NULL,
  `DPAUraian` text,
  `DPAIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `LK11` text,
  `LK12` text,
  `LK13` text,
  `LK14` text,
  `LK21` text,
  `LK22` text,
  `LK23` text,
  `LK24` text,
  `LK31` text,
  `LK32` text,
  `LK4` text,
  PRIMARY KEY (`DPAId`),
  KEY `FK_DPARENSTRA_RENSTRA` (`IdRenstra`),
  CONSTRAINT `FK_DPARENSTRA_RENSTRA` FOREIGN KEY (`IdRenstra`) REFERENCES `sakipv2_skpd_renstra` (`RenstraId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd_renstra_dpa` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd_renstra_dpa` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd_renstra_dpa` (`IdRenstra`, `DPAId`, `DPATahun`, `DPAUraian`, `DPAIsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `LK11`, `LK12`, `LK13`, `LK14`, `LK21`, `LK22`, `LK23`, `LK24`, `LK31`, `LK32`, `LK4`)
VALUES
	(2,1,2024,'DPA INDUK',1,'admin','2024-04-03 00:32:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd_renstra_dpa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_renstra_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasaran`;

CREATE TABLE `sakipv2_skpd_renstra_sasaran` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `IdSasaranPmd` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) NOT NULL,
  `SasaranUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_RENSTRASASARAN_TUJUAN` (`IdTujuan`),
  KEY `FK_RENSTRASASARAN_PMDSASARAN` (`IdSasaranPmd`),
  CONSTRAINT `FK_RENSTRASASARAN_PMDSASARAN` FOREIGN KEY (`IdSasaranPmd`) REFERENCES `sakipv2_pemda_sasaran` (`SasaranId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_RENSTRASASARAN_TUJUAN` FOREIGN KEY (`IdTujuan`) REFERENCES `sakipv2_skpd_renstra_tujuan` (`TujuanId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd_renstra_sasaran` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd_renstra_sasaran` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd_renstra_sasaran` (`IdTujuan`, `IdSasaranPmd`, `SasaranId`, `SasaranNo`, `SasaranUraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,1,1,'Sasaran 01','admin','2024-04-03 00:36:32',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd_renstra_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_renstra_sasarandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasarandet`;

CREATE TABLE `sakipv2_skpd_renstra_sasarandet` (
  `IdSasaran` bigint(20) unsigned NOT NULL,
  `SsrIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SsrIndikatorUraian` text,
  `SsrIndikatorSumberData` text,
  `SsrIndikatorFormulasi` text,
  `SsrIndikatorSatuan` text,
  `SsrIndikatorTarget` text,
  `SsrIndikatorRealisasi` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SsrIndikatorId`),
  KEY `FK_RENSTRASASARANDET_SASARAN` (`IdSasaran`),
  CONSTRAINT `FK_RENSTRASASARANDET_SASARAN` FOREIGN KEY (`IdSasaran`) REFERENCES `sakipv2_skpd_renstra_sasaran` (`SasaranId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd_renstra_sasarandet` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd_renstra_sasarandet` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd_renstra_sasarandet` (`IdSasaran`, `SsrIndikatorId`, `SsrIndikatorUraian`, `SsrIndikatorSumberData`, `SsrIndikatorFormulasi`, `SsrIndikatorSatuan`, `SsrIndikatorTarget`, `SsrIndikatorRealisasi`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,'Lorem Ipsum','Lorem Ipsum','Lorem Ipsum','Dokumen','10',NULL,'admin','2024-04-03 00:36:32',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd_renstra_sasarandet` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_renstra_sasaranmonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasaranmonev`;

CREATE TABLE `sakipv2_skpd_renstra_sasaranmonev` (
  `IdSasaranIndikator` bigint(20) unsigned DEFAULT NULL,
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MonevTahun` int(11) NOT NULL,
  `MonevTarget` text,
  `MonevTargetTW1` text,
  `MonevTargetTW2` text,
  `MonevTargetTW3` text,
  `MonevTargetTW4` text,
  `MonevRealisasi` text,
  `MonevRealisasiTW1` text,
  `MonevRealisasiTW2` text,
  `MonevRealisasiTW3` text,
  `MonevRealisasiTW4` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_MONEV_OPDSASARAN` (`IdSasaranIndikator`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_tujuan`;

CREATE TABLE `sakipv2_skpd_renstra_tujuan` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `IdTujuanPmd` bigint(20) unsigned NOT NULL,
  `TujuanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujuanNo` int(11) NOT NULL,
  `TujuanUraian` text NOT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujuanId`),
  KEY `FK_TUJUAN_RENSTRA` (`IdRenstra`),
  KEY `FK_RENSTRATUJUAN_PMDTUJUAN` (`IdTujuanPmd`),
  CONSTRAINT `FK_RENSTRATUJUAN_PMDTUJUAN` FOREIGN KEY (`IdTujuanPmd`) REFERENCES `sakipv2_pemda_tujuan` (`TujuanId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TUJUAN_RENSTRA` FOREIGN KEY (`IdRenstra`) REFERENCES `sakipv2_skpd_renstra` (`RenstraId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd_renstra_tujuan` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd_renstra_tujuan` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd_renstra_tujuan` (`IdRenstra`, `IdTujuanPmd`, `TujuanId`, `TujuanNo`, `TujuanUraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,2,1,1,'Tujuan 1','admin','2024-04-03 00:31:31',NULL,NULL),
	(2,2,2,2,'Tujuan 2','admin','2024-04-03 00:31:53',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd_renstra_tujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_renstra_tujuandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_tujuandet`;

CREATE TABLE `sakipv2_skpd_renstra_tujuandet` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `TujIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujIndikatorUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujIndikatorId`),
  KEY `FK_RENSTUJUANDET_TUJUAN` (`IdTujuan`),
  CONSTRAINT `FK_RENSTUJUANDET_TUJUAN` FOREIGN KEY (`IdTujuan`) REFERENCES `sakipv2_skpd_renstra_tujuan` (`TujuanId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid`;

CREATE TABLE `sakipv2_subbid` (
  `IdBid` bigint(20) DEFAULT NULL,
  `SubbidId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SubbidNama` varchar(200) NOT NULL DEFAULT '',
  `SubbidNamaPimpinan` varchar(200) DEFAULT NULL,
  `SubbidNamaJabatan` varchar(200) DEFAULT NULL,
  `SubbidIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `SubbidTugasPokok` text,
  `SubbidFungsi` text,
  `SubbidIKU` text,
  `SubbidAtasan` varchar(200) DEFAULT NULL,
  `SubbidAtasanNama` varchar(200) DEFAULT NULL,
  `SubbidAtasanJab` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SubbidId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_pelaksana
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_pelaksana`;

CREATE TABLE `sakipv2_subbid_pelaksana` (
  `IdSubbid` bigint(20) NOT NULL,
  `PlsId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PlsNama` varchar(200) DEFAULT NULL,
  `PlsNamaPegawai` varchar(200) DEFAULT NULL,
  `PlsTugasPokok` text,
  `PlsFungsi` text,
  `PlsIKU` text,
  `PlsAtasan` varchar(200) DEFAULT NULL,
  `PlsAtasanNama` varchar(200) DEFAULT NULL,
  `PlsAtasanJab` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PlsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_subkegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_subkegiatan`;

CREATE TABLE `sakipv2_subbid_subkegiatan` (
  `IdSubbid` bigint(20) DEFAULT NULL,
  `IdKegiatan` bigint(20) unsigned NOT NULL,
  `SubkegId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SubkegKode` varchar(200) DEFAULT NULL,
  `SubkegUraian` text,
  `SubkegPagu` double DEFAULT NULL,
  `SubkegRealisasi` double DEFAULT NULL,
  `SubkegPaguTW1` double DEFAULT NULL,
  `SubkegPaguTW2` double DEFAULT NULL,
  `SubkegPaguTW3` double DEFAULT NULL,
  `SubkegPaguTW4` double DEFAULT NULL,
  `SubkegRealisasiTW1` double DEFAULT NULL,
  `SubkegRealisasiTW2` double DEFAULT NULL,
  `SubkegRealisasiTW3` double DEFAULT NULL,
  `SubkegRealisasiTW4` double DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SubkegId`),
  KEY `FK_SUBKEG_KEGIATAN` (`IdKegiatan`),
  CONSTRAINT `FK_SUBKEG_KEGIATAN` FOREIGN KEY (`IdKegiatan`) REFERENCES `sakipv2_bid_kegiatan` (`KegiatanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_subkegsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_subkegsasaran`;

CREATE TABLE `sakipv2_subbid_subkegsasaran` (
  `IdSubkeg` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_SUBKEGSASARAN_SUBKEG` (`IdSubkeg`),
  CONSTRAINT `FK_SUBKEGSASARAN_SUBKEG` FOREIGN KEY (`IdSubkeg`) REFERENCES `sakipv2_subbid_subkegiatan` (`SubkegId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ta_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_kegiatan`;

CREATE TABLE `ta_kegiatan` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL,
  `Kd_Bidang` tinyint(4) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `Kd_Keg` int(11) NOT NULL,
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Kegiatan` varchar(255) DEFAULT NULL,
  `Lokasi` varchar(800) DEFAULT NULL,
  `Kelompok_Sasaran` varchar(255) DEFAULT NULL,
  `Status_Kegiatan` varchar(1) NOT NULL COMMENT '1. baru, 2 lanjutan',
  `Pagu_Anggaran` double DEFAULT NULL,
  `Waktu_Pelaksanaan` varchar(100) DEFAULT NULL,
  `Kd_Sumber` tinyint(4) DEFAULT NULL,
  `Status` int(11) NOT NULL,
  `Keterangan` text NOT NULL,
  `Pagu_Anggaran_Nt1` double DEFAULT NULL,
  `Verifikasi_Bappeda` tinyint(4) DEFAULT NULL,
  `Tanggal_Verifikasi_Bappeda` int(11) DEFAULT NULL,
  `Keterangan_Verifikasi_Bappeda` mediumtext,
  `Kd_Ref` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`,`Kd_Keg`) USING BTREE,
  KEY `Ta_Kegiatan` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`,`Kd_Keg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ta_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_program`;

CREATE TABLE `ta_program` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL COMMENT 'untuk filter / kode skpd',
  `Kd_Bidang` tinyint(4) NOT NULL COMMENT 'untuk filter / koda skpd',
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Prog` varchar(255) NOT NULL,
  `Tolak_Ukur` varchar(255) DEFAULT NULL,
  `Target_Angka` double DEFAULT NULL,
  `Target_Uraian` varchar(255) DEFAULT NULL,
  `Kd_Urusan1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  `Kd_Bidang1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`),
  KEY `FK_Ta_Program` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
